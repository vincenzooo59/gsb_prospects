-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 28 mai 2019 à 14:09
-- Version du serveur :  5.7.24
-- Version de PHP :  7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `gsb_cr`
--

DELIMITER $$
--
-- Fonctions
--
DROP FUNCTION IF EXISTS `dm`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `dm` (`st` VARCHAR(55)) RETURNS VARCHAR(128) CHARSET utf8 NO SQL
BEGIN
	DECLARE length, first, last, pos, prevpos, is_slavo_germanic SMALLINT;
	DECLARE pri, sec VARCHAR(45) DEFAULT '';
	DECLARE ch CHAR(1);
	
	
	
	
	SET first = 3;
	SET length = CHAR_LENGTH(st);
	SET last = first + length -1;
	SET st = CONCAT(REPEAT('-', first -1), UCASE(st), REPEAT(' ', 5)); 
	SET is_slavo_germanic = (st LIKE '%W%' OR st LIKE '%K%' OR st LIKE '%CZ%');  
	SET pos = first; 
	
	IF SUBSTRING(st, first, 2) IN ('GN', 'KN', 'PN', 'WR', 'PS') THEN
		SET pos = pos + 1;
	END IF;
	
	IF SUBSTRING(st, first, 1) = 'X' THEN
		SET pri = 'S', sec = 'S', pos = pos  + 1; 
	END IF;
	
	WHILE pos <= last DO
		
    SET prevpos = pos;
		SET ch = SUBSTRING(st, pos, 1); 
		CASE
		WHEN ch IN ('A', 'E', 'I', 'O', 'U', 'Y') THEN
			IF pos = first THEN 
				SET pri = CONCAT(pri, 'A'), sec = CONCAT(sec, 'A'), pos = pos  + 1; 
			ELSE
				SET pos = pos + 1;
			END IF;
		WHEN ch = 'B' THEN
			
			IF SUBSTRING(st, pos+1, 1) = 'B' THEN
				SET pri = CONCAT(pri, 'P'), sec = CONCAT(sec, 'P'), pos = pos  + 2; 
			ELSE
				SET pri = CONCAT(pri, 'P'), sec = CONCAT(sec, 'P'), pos = pos  + 1; 
			END IF;
		WHEN ch = 'C' THEN
			
			IF (pos > (first + 1) AND SUBSTRING(st, pos-2, 1) NOT IN ('A', 'E', 'I', 'O', 'U', 'Y') AND SUBSTRING(st, pos-1, 3) = 'ACH' AND
			   (SUBSTRING(st, pos+2, 1) NOT IN ('I', 'E') OR SUBSTRING(st, pos-2, 6) IN ('BACHER', 'MACHER'))) THEN
				SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 2; 
			
			ELSEIF pos = first AND SUBSTRING(st, first, 6) = 'CAESAR' THEN
				SET pri = CONCAT(pri, 'S'), sec = CONCAT(sec, 'S'), pos = pos  + 2; 
			ELSEIF SUBSTRING(st, pos, 4) = 'CHIA' THEN 
				SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 2; 
			ELSEIF SUBSTRING(st, pos, 2) = 'CH' THEN
				
				IF pos > first AND SUBSTRING(st, pos, 4) = 'CHAE' THEN
					SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'X'), pos = pos  + 2; 
				ELSEIF pos = first AND (SUBSTRING(st, pos+1, 5) IN ('HARAC', 'HARIS') OR
				   SUBSTRING(st, pos+1, 3) IN ('HOR', 'HYM', 'HIA', 'HEM')) AND SUBSTRING(st, first, 5) != 'CHORE' THEN
					SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 2; 
				
				ELSEIF SUBSTRING(st, first, 4) IN ('VAN ', 'VON ') OR SUBSTRING(st, first, 3) = 'SCH'
				   OR SUBSTRING(st, pos-2, 6) IN ('ORCHES', 'ARCHIT', 'ORCHID')
				   OR SUBSTRING(st, pos+2, 1) IN ('T', 'S')
				   OR ((SUBSTRING(st, pos-1, 1) IN ('A', 'O', 'U', 'E') OR pos = first)
				   AND SUBSTRING(st, pos+2, 1) IN ('L', 'R', 'N', 'M', 'B', 'H', 'F', 'V', 'W', ' ')) THEN
					SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 2; 
				ELSE
					IF pos > first THEN
						IF SUBSTRING(st, first, 2) = 'MC' THEN
							SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 2; 
						ELSE
							SET pri = CONCAT(pri, 'X'), sec = CONCAT(sec, 'K'), pos = pos  + 2; 
						END IF;
					ELSE
						SET pri = CONCAT(pri, 'X'), sec = CONCAT(sec, 'X'), pos = pos  + 2; 
					END IF;
				END IF;
			
			ELSEIF SUBSTRING(st, pos, 2) = 'CZ' AND SUBSTRING(st, pos-2, 4) != 'WICZ' THEN
				SET pri = CONCAT(pri, 'S'), sec = CONCAT(sec, 'X'), pos = pos  + 2; 
			
			ELSEIF SUBSTRING(st, pos+1, 3) = 'CIA' THEN
				SET pri = CONCAT(pri, 'X'), sec = CONCAT(sec, 'X'), pos = pos  + 3; 
			
			ELSEIF SUBSTRING(st, pos, 2) = 'CC' AND NOT (pos = (first +1) AND SUBSTRING(st, first, 1) = 'M') THEN
				
				IF SUBSTRING(st, pos+2, 1) IN ('I', 'E', 'H') AND SUBSTRING(st, pos+2, 2) != 'HU' THEN
					
					IF (pos = first +1 AND SUBSTRING(st, first) = 'A') OR
					   SUBSTRING(st, pos-1, 5) IN ('UCCEE', 'UCCES') THEN
						SET pri = CONCAT(pri, 'KS'), sec = CONCAT(sec, 'KS'), pos = pos  + 3; 
					
					ELSE
						SET pri = CONCAT(pri, 'X'), sec = CONCAT(sec, 'X'), pos = pos  + 3; 
					END IF;
				ELSE
					SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 2; 
				END IF;
			ELSEIF SUBSTRING(st, pos, 2) IN ('CK', 'CG', 'CQ') THEN
				SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 2; 
			ELSEIF SUBSTRING(st, pos, 2) IN ('CI', 'CE', 'CY') THEN
				
				IF SUBSTRING(st, pos, 3) IN ('CIO', 'CIE', 'CIA') THEN
					SET pri = CONCAT(pri, 'S'), sec = CONCAT(sec, 'X'), pos = pos  + 2; 
				ELSE
					SET pri = CONCAT(pri, 'S'), sec = CONCAT(sec, 'S'), pos = pos  + 2; 
				END IF;
			ELSE
				
				IF SUBSTRING(st, pos+1, 2) IN (' C', ' Q', ' G') THEN
					SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 3; 
				ELSE
					IF SUBSTRING(st, pos+1, 1) IN ('C', 'K', 'Q') AND SUBSTRING(st, pos+1, 2) NOT IN ('CE', 'CI') THEN
						SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 2; 
					ELSE 
						SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 1; 
					END IF;
				END IF;
			END IF;
		
			
		WHEN ch = 'D' THEN
			IF SUBSTRING(st, pos, 2) = 'DG' THEN
				IF SUBSTRING(st, pos+2, 1) IN ('I', 'E', 'Y') THEN 
					SET pri = CONCAT(pri, 'J'), sec = CONCAT(sec, 'J'), pos = pos  + 3; 
				ELSE
					SET pri = CONCAT(pri, 'TK'), sec = CONCAT(sec, 'TK'), pos = pos  + 2; 
				END IF;
			ELSEIF SUBSTRING(st, pos, 2) IN ('DT', 'DD') THEN
				SET pri = CONCAT(pri, 'T'), sec = CONCAT(sec, 'T'), pos = pos  + 2; 
			ELSE
				SET pri = CONCAT(pri, 'T'), sec = CONCAT(sec, 'T'), pos = pos  + 1; 
			END IF;
		WHEN ch = 'F' THEN
			IF SUBSTRING(st, pos+1, 1) = 'F' THEN
				SET pri = CONCAT(pri, 'F'), sec = CONCAT(sec, 'F'), pos = pos  + 2; 
			ELSE
				SET pri = CONCAT(pri, 'F'), sec = CONCAT(sec, 'F'), pos = pos  + 1; 
			END IF;
		WHEN ch = 'G' THEN
			IF SUBSTRING(st, pos+1, 1) = 'H' THEN
				IF (pos > first AND SUBSTRING(st, pos-1, 1) NOT IN ('A', 'E', 'I', 'O', 'U', 'Y'))
					OR ( pos = first AND SUBSTRING(st, pos+2, 1) != 'I') THEN
					SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 2; 
				ELSEIF pos = first AND SUBSTRING(st, pos+2, 1) = 'I' THEN
					 SET pri = CONCAT(pri, 'J'), sec = CONCAT(sec, 'J'), pos = pos  + 2; 
				
				ELSEIF (pos > (first + 1) AND SUBSTRING(st, pos-2, 1) IN ('B', 'H', 'D') )
				   OR (pos > (first + 2) AND SUBSTRING(st, pos-3, 1) IN ('B', 'H', 'D') )
				   OR (pos > (first + 3) AND SUBSTRING(st, pos-4, 1) IN ('B', 'H') ) THEN
					SET pos = pos + 2; 
				ELSE
					
					IF pos > (first + 2) AND SUBSTRING(st, pos-1, 1) = 'U'
					   AND SUBSTRING(st, pos-3, 1) IN ('C', 'G', 'L', 'R', 'T') THEN
						SET pri = CONCAT(pri, 'F'), sec = CONCAT(sec, 'F'), pos = pos  + 2; 
					ELSEIF pos > first AND SUBSTRING(st, pos-1, 1) != 'I' THEN
						SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 2; 
          ELSE
              SET pos = pos + 1;
					END IF;
				END IF;
			ELSEIF SUBSTRING(st, pos+1, 1) = 'N' THEN
				IF pos = (first +1) AND SUBSTRING(st, first, 1) IN ('A', 'E', 'I', 'O', 'U', 'Y') AND NOT is_slavo_germanic THEN
					SET pri = CONCAT(pri, 'KN'), sec = CONCAT(sec, 'N'), pos = pos  + 2; 
				ELSE
					
					IF SUBSTRING(st, pos+2, 2) != 'EY' AND SUBSTRING(st, pos+1, 1) != 'Y'
						AND NOT is_slavo_germanic THEN
						SET pri = CONCAT(pri, 'N'), sec = CONCAT(sec, 'KN'), pos = pos  + 2; 
					ELSE
						SET pri = CONCAT(pri, 'KN'), sec = CONCAT(sec, 'KN'), pos = pos  + 2; 
					END IF;
				END IF;
			
			ELSEIF SUBSTRING(st, pos+1, 2) = 'LI' AND NOT is_slavo_germanic THEN
				SET pri = CONCAT(pri, 'KL'), sec = CONCAT(sec, 'L'), pos = pos  + 2; 
			
			ELSEIF pos = first AND (SUBSTRING(st, pos+1, 1) = 'Y'
			   OR SUBSTRING(st, pos+1, 2) IN ('ES', 'EP', 'EB', 'EL', 'EY', 'IB', 'IL', 'IN', 'IE', 'EI', 'ER')) THEN
				SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'J'), pos = pos  + 2; 
			
			ELSEIF (SUBSTRING(st, pos+1, 2) = 'ER' OR SUBSTRING(st, pos+1, 1) = 'Y')
			   AND SUBSTRING(st, first, 6) NOT IN ('DANGER', 'RANGER', 'MANGER')
			   AND SUBSTRING(st, pos-1, 1) not IN ('E', 'I') AND SUBSTRING(st, pos-1, 3) NOT IN ('RGY', 'OGY') THEN
				SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'J'), pos = pos  + 2; 
			
			ELSEIF SUBSTRING(st, pos+1, 1) IN ('E', 'I', 'Y') OR SUBSTRING(st, pos-1, 4) IN ('AGGI', 'OGGI') THEN
				
				IF SUBSTRING(st, first, 4) IN ('VON ', 'VAN ') OR SUBSTRING(st, first, 3) = 'SCH'
				   OR SUBSTRING(st, pos+1, 2) = 'ET' THEN
					SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 2; 
				ELSE
					
					IF SUBSTRING(st, pos+1, 4) = 'IER ' THEN
						SET pri = CONCAT(pri, 'J'), sec = CONCAT(sec, 'J'), pos = pos  + 2; 
					ELSE
						SET pri = CONCAT(pri, 'J'), sec = CONCAT(sec, 'K'), pos = pos  + 2; 
					END IF;
				END IF;
			ELSEIF SUBSTRING(st, pos+1, 1) = 'G' THEN
				SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 2; 
			ELSE
				SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 1; 
			END IF;
		WHEN ch = 'H' THEN
			
			IF (pos = first OR SUBSTRING(st, pos-1, 1) IN ('A', 'E', 'I', 'O', 'U', 'Y'))
				AND SUBSTRING(st, pos+1, 1) IN ('A', 'E', 'I', 'O', 'U', 'Y') THEN
				SET pri = CONCAT(pri, 'H'), sec = CONCAT(sec, 'H'), pos = pos  + 2; 
			ELSE 
				SET pos = pos + 1; 
			END IF;
		WHEN ch = 'J' THEN
			
			IF SUBSTRING(st, pos, 4) = 'JOSE' OR SUBSTRING(st, first, 4) = 'SAN ' THEN
				IF (pos = first AND SUBSTRING(st, pos+4, 1) = ' ') OR SUBSTRING(st, first, 4) = 'SAN ' THEN
					SET pri = CONCAT(pri, 'H'), sec = CONCAT(sec, 'H'); 
				ELSE
					SET pri = CONCAT(pri, 'J'), sec = CONCAT(sec, 'H'); 
				END IF;
			ELSEIF pos = first AND SUBSTRING(st, pos, 4) != 'JOSE' THEN
				SET pri = CONCAT(pri, 'J'), sec = CONCAT(sec, 'A'); 
			ELSE
				
				IF SUBSTRING(st, pos-1, 1) IN ('A', 'E', 'I', 'O', 'U', 'Y') AND NOT is_slavo_germanic
				   AND SUBSTRING(st, pos+1, 1) IN ('A', 'O') THEN
					SET pri = CONCAT(pri, 'J'), sec = CONCAT(sec, 'H'); 
				ELSE
					IF pos = last THEN
						SET pri = CONCAT(pri, 'J'); 
					ELSE
						IF SUBSTRING(st, pos+1, 1) not IN ('L', 'T', 'K', 'S', 'N', 'M', 'B', 'Z')
						   AND SUBSTRING(st, pos-1, 1) not IN ('S', 'K', 'L') THEN
							SET pri = CONCAT(pri, 'J'), sec = CONCAT(sec, 'J'); 
						END IF;
					END IF;
				END IF;
			END IF;
			IF SUBSTRING(st, pos+1, 1) = 'J' THEN
				SET pos = pos + 2;
			ELSE
				SET pos = pos + 1;
			END IF;
		WHEN ch = 'K' THEN
			IF SUBSTRING(st, pos+1, 1) = 'K' THEN
				SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 2; 
			ELSE
				SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 1; 
			END IF;
		WHEN ch = 'L' THEN
			IF SUBSTRING(st, pos+1, 1) = 'L' THEN
				
				IF (pos = (last - 2) AND SUBSTRING(st, pos-1, 4) IN ('ILLO', 'ILLA', 'ALLE'))
				   OR ((SUBSTRING(st, last-1, 2) IN ('AS', 'OS') OR SUBSTRING(st, last) IN ('A', 'O'))
				   AND SUBSTRING(st, pos-1, 4) = 'ALLE') THEN
					SET pri = CONCAT(pri, 'L'), pos = pos  + 2; 
				ELSE
					SET pri = CONCAT(pri, 'L'), sec = CONCAT(sec, 'L'), pos = pos  + 2; 
				END IF;
			ELSE
				SET pri = CONCAT(pri, 'L'), sec = CONCAT(sec, 'L'), pos = pos  + 1; 
			END IF;
		WHEN ch = 'M' THEN
			IF SUBSTRING(st, pos-1, 3) = 'UMB'
			   AND (pos + 1 = last OR SUBSTRING(st, pos+2, 2) = 'ER')
			   OR SUBSTRING(st, pos+1, 1) = 'M' THEN
				SET pri = CONCAT(pri, 'M'), sec = CONCAT(sec, 'M'), pos = pos  + 2; 
			ELSE
				SET pri = CONCAT(pri, 'M'), sec = CONCAT(sec, 'M'), pos = pos  + 1; 
			END IF;
		WHEN ch = 'N' THEN
			IF SUBSTRING(st, pos+1, 1) = 'N' THEN
				SET pri = CONCAT(pri, 'N'), sec = CONCAT(sec, 'N'), pos = pos  + 2; 
			ELSE
				SET pri = CONCAT(pri, 'N'), sec = CONCAT(sec, 'N'), pos = pos  + 1; 
			END IF;
		
			
		WHEN ch = 'P' THEN
			IF SUBSTRING(st, pos+1, 1) = 'H' THEN
				SET pri = CONCAT(pri, 'F'), sec = CONCAT(sec, 'F'), pos = pos  + 2; 
			ELSEIF SUBSTRING(st, pos+1, 1) IN ('P', 'B') THEN 
				SET pri = CONCAT(pri, 'P'), sec = CONCAT(sec, 'P'), pos = pos  + 2; 
			ELSE
				SET pri = CONCAT(pri, 'P'), sec = CONCAT(sec, 'P'), pos = pos  + 1; 
			END IF;
		WHEN ch = 'Q' THEN
			IF SUBSTRING(st, pos+1, 1) = 'Q' THEN
				SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 2; 
			ELSE
				SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 1; 
			END IF;
		WHEN ch = 'R' THEN
			
			IF pos = last AND not is_slavo_germanic
			   AND SUBSTRING(st, pos-2, 2) = 'IE' AND SUBSTRING(st, pos-4, 2) NOT IN ('ME', 'MA') THEN
				SET sec = CONCAT(sec, 'R'); 
			ELSE
				SET pri = CONCAT(pri, 'R'), sec = CONCAT(sec, 'R'); 
			END IF;
			IF SUBSTRING(st, pos+1, 1) = 'R' THEN
				SET pos = pos + 2;
			ELSE
				SET pos = pos + 1;
			END IF;
		WHEN ch = 'S' THEN
			
			IF SUBSTRING(st, pos-1, 3) IN ('ISL', 'YSL') THEN
				SET pos = pos + 1;
			
			ELSEIF pos = first AND SUBSTRING(st, first, 5) = 'SUGAR' THEN
				SET pri = CONCAT(pri, 'X'), sec = CONCAT(sec, 'S'), pos = pos  + 1; 
			ELSEIF SUBSTRING(st, pos, 2) = 'SH' THEN
				
				IF SUBSTRING(st, pos+1, 4) IN ('HEIM', 'HOEK', 'HOLM', 'HOLZ') THEN
					SET pri = CONCAT(pri, 'S'), sec = CONCAT(sec, 'S'), pos = pos  + 2; 
				ELSE
					SET pri = CONCAT(pri, 'X'), sec = CONCAT(sec, 'X'), pos = pos  + 2; 
				END IF;
			
			ELSEIF SUBSTRING(st, pos, 3) IN ('SIO', 'SIA') OR SUBSTRING(st, pos, 4) = 'SIAN' THEN
				IF NOT is_slavo_germanic THEN
					SET pri = CONCAT(pri, 'S'), sec = CONCAT(sec, 'X'), pos = pos  + 3; 
				ELSE
					SET pri = CONCAT(pri, 'S'), sec = CONCAT(sec, 'S'), pos = pos  + 3; 
				END IF;
			
			
			ELSEIF (pos = first AND SUBSTRING(st, pos+1, 1) IN ('M', 'N', 'L', 'W')) OR SUBSTRING(st, pos+1, 1) = 'Z' THEN
				SET pri = CONCAT(pri, 'S'), sec = CONCAT(sec, 'X'); 
				IF SUBSTRING(st, pos+1, 1) = 'Z' THEN
					SET pos = pos + 2;
				ELSE
					SET pos = pos + 1;
				END IF;
			ELSEIF SUBSTRING(st, pos, 2) = 'SC' THEN
				
				IF SUBSTRING(st, pos+2, 1) = 'H' THEN
					
					IF SUBSTRING(st, pos+3, 2) IN ('OO', 'ER', 'EN', 'UY', 'ED', 'EM') THEN
						
						IF SUBSTRING(st, pos+3, 2) IN ('ER', 'EN') THEN
							SET pri = CONCAT(pri, 'X'), sec = CONCAT(sec, 'SK'), pos = pos  + 3; 
						ELSE
							SET pri = CONCAT(pri, 'SK'), sec = CONCAT(sec, 'SK'), pos = pos  + 3; 
						END IF;
					ELSE
						IF pos = first AND SUBSTRING(st, first+3, 1) not IN ('A', 'E', 'I', 'O', 'U', 'Y') AND SUBSTRING(st, first+3, 1) != 'W' THEN
							SET pri = CONCAT(pri, 'X'), sec = CONCAT(sec, 'S'), pos = pos  + 3; 
						ELSE
							SET pri = CONCAT(pri, 'X'), sec = CONCAT(sec, 'X'), pos = pos  + 3; 
						END IF;
					END IF;
				ELSEIF SUBSTRING(st, pos+2, 1) IN ('I', 'E', 'Y') THEN
					SET pri = CONCAT(pri, 'S'), sec = CONCAT(sec, 'S'), pos = pos  + 3; 
				ELSE
					SET pri = CONCAT(pri, 'SK'), sec = CONCAT(sec, 'SK'), pos = pos  + 3; 
				END IF;
			
			ELSEIF pos = last AND SUBSTRING(st, pos-2, 2) IN ('AI', 'OI') THEN
				SET sec = CONCAT(sec, 'S'), pos = pos  + 1; 
			ELSE
				SET pri = CONCAT(pri, 'S'), sec = CONCAT(sec, 'S'); 
				IF SUBSTRING(st, pos+1, 1) IN ('S', 'Z') THEN
					SET pos = pos + 2;
				ELSE
					SET pos = pos + 1;
				END IF;
			END IF;
		WHEN ch = 'T' THEN
			IF SUBSTRING(st, pos, 4) = 'TION' THEN
				SET pri = CONCAT(pri, 'X'), sec = CONCAT(sec, 'X'), pos = pos  + 3; 
			ELSEIF SUBSTRING(st, pos, 3) IN ('TIA', 'TCH') THEN
				SET pri = CONCAT(pri, 'X'), sec = CONCAT(sec, 'X'), pos = pos  + 3; 
			ELSEIF SUBSTRING(st, pos, 2) = 'TH' OR SUBSTRING(st, pos, 3) = 'TTH' THEN
				
				IF SUBSTRING(st, pos+2, 2) IN ('OM', 'AM') OR SUBSTRING(st, first, 4) IN ('VON ', 'VAN ')
				   OR SUBSTRING(st, first, 3) = 'SCH' THEN
					SET pri = CONCAT(pri, 'T'), sec = CONCAT(sec, 'T'), pos = pos  + 2; 
				ELSE
					SET pri = CONCAT(pri, '0'), sec = CONCAT(sec, 'T'), pos = pos  + 2; 
				END IF;
			ELSEIF SUBSTRING(st, pos+1, 1) IN ('T', 'D') THEN
				SET pri = CONCAT(pri, 'T'), sec = CONCAT(sec, 'T'), pos = pos  + 2; 
			ELSE
				SET pri = CONCAT(pri, 'T'), sec = CONCAT(sec, 'T'), pos = pos  + 1; 
			END IF;
		WHEN ch = 'V' THEN
			IF SUBSTRING(st, pos+1, 1) = 'V' THEN
				SET pri = CONCAT(pri, 'F'), sec = CONCAT(sec, 'F'), pos = pos  + 2; 
			ELSE
				SET pri = CONCAT(pri, 'F'), sec = CONCAT(sec, 'F'), pos = pos  + 1; 
			END IF;
		WHEN ch = 'W' THEN
			
			IF SUBSTRING(st, pos, 2) = 'WR' THEN
				SET pri = CONCAT(pri, 'R'), sec = CONCAT(sec, 'R'), pos = pos  + 2; 
			ELSEIF pos = first AND (SUBSTRING(st, pos+1, 1) IN ('A', 'E', 'I', 'O', 'U', 'Y')
				OR SUBSTRING(st, pos, 2) = 'WH') THEN
				
				IF SUBSTRING(st, pos+1, 1) IN ('A', 'E', 'I', 'O', 'U', 'Y') THEN
					SET pri = CONCAT(pri, 'A'), sec = CONCAT(sec, 'F'), pos = pos  + 1; 
				ELSE
					SET pri = CONCAT(pri, 'A'), sec = CONCAT(sec, 'A'), pos = pos  + 1; 
				END IF;
			
			ELSEIF (pos = last AND SUBSTRING(st, pos-1, 1) IN ('A', 'E', 'I', 'O', 'U', 'Y'))
			   OR SUBSTRING(st, pos-1, 5) IN ('EWSKI', 'EWSKY', 'OWSKI', 'OWSKY')
			   OR SUBSTRING(st, first, 3) = 'SCH' THEN
				SET sec = CONCAT(sec, 'F'), pos = pos  + 1; 
			
			
			ELSEIF SUBSTRING(st, pos, 4) IN ('WICZ', 'WITZ') THEN
				SET pri = CONCAT(pri, 'TS'), sec = CONCAT(sec, 'FX'), pos = pos  + 4; 
			ELSE 
				SET pos = pos + 1;
			END IF;
		WHEN ch = 'X' THEN
			
			IF not(pos = last AND (SUBSTRING(st, pos-3, 3) IN ('IAU', 'EAU')
			   OR SUBSTRING(st, pos-2, 2) IN ('AU', 'OU'))) THEN
				SET pri = CONCAT(pri, 'KS'), sec = CONCAT(sec, 'KS'); 
			END IF;
			IF SUBSTRING(st, pos+1, 1) IN ('C', 'X') THEN
				SET pos = pos + 2;
			ELSE
				SET pos = pos + 1;
			END IF;
		WHEN ch = 'Z' THEN
			
			IF SUBSTRING(st, pos+1, 1) = 'H' THEN
				SET pri = CONCAT(pri, 'J'), sec = CONCAT(sec, 'J'), pos = pos  + 1; 
			ELSEIF SUBSTRING(st, pos+1, 3) IN ('ZO', 'ZI', 'ZA')
			   OR (is_slavo_germanic AND pos > first AND SUBSTRING(st, pos-1, 1) != 'T') THEN
				SET pri = CONCAT(pri, 'S'), sec = CONCAT(sec, 'TS'); 
			ELSE
				SET pri = CONCAT(pri, 'S'), sec = CONCAT(sec, 'S'); 
			END IF;
			IF SUBSTRING(st, pos+1, 1) = 'Z' THEN
				SET pos = pos + 2;
			ELSE
				SET pos = pos + 1;
			END IF;
		ELSE
			SET pos = pos + 1; 
		END CASE;
    IF pos = prevpos THEN
       SET pos = pos +1;
       SET pri = CONCAT(pri,'<didnt incr>'); 
    END IF;
	END WHILE;
	IF pri != sec THEN
		SET pri = CONCAT(pri, ';', sec);
  END IF;
	RETURN (pri);
END$$

DROP FUNCTION IF EXISTS `metaphone`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `metaphone` (`st` VARCHAR(55)) RETURNS VARCHAR(128) CHARSET latin1 NO SQL
    DETERMINISTIC
BEGIN
	DECLARE length, first, last, pos, prevpos, is_slavo_germanic SMALLINT;
	DECLARE pri, sec VARCHAR(45) DEFAULT '';
	DECLARE ch CHAR(1);
	
	
	
	
	SET first = 3;
	SET length = CHAR_LENGTH(st);
	SET last = first + length -1;
	SET st = CONCAT(REPEAT('-', first -1), UCASE(st), REPEAT(' ', 5)); 
	SET is_slavo_germanic = (st LIKE '%W%' OR st LIKE '%K%' OR st LIKE '%CZ%');  
	SET pos = first; 
	
	IF SUBSTRING(st, first, 2) IN ('GN', 'KN', 'PN', 'WR', 'PS') THEN
		SET pos = pos + 1;
	END IF;
	
	IF SUBSTRING(st, first, 1) = 'X' THEN
		SET pri = 'S', sec = 'S', pos = pos  + 1; 
	END IF;
	
	WHILE pos <= last DO
		
    SET prevpos = pos;
		SET ch = SUBSTRING(st, pos, 1); 
		CASE
		WHEN ch IN ('A', 'E', 'I', 'O', 'U', 'Y') THEN
			IF pos = first THEN 
				SET pri = CONCAT(pri, 'A'), sec = CONCAT(sec, 'A'), pos = pos  + 1; 
			ELSE
				SET pos = pos + 1;
			END IF;
		WHEN ch = 'B' THEN
			
			IF SUBSTRING(st, pos+1, 1) = 'B' THEN
				SET pri = CONCAT(pri, 'P'), sec = CONCAT(sec, 'P'), pos = pos  + 2; 
			ELSE
				SET pri = CONCAT(pri, 'P'), sec = CONCAT(sec, 'P'), pos = pos  + 1; 
			END IF;
		WHEN ch = 'C' THEN
			
			IF (pos > (first + 1) AND SUBSTRING(st, pos-2, 1) NOT IN ('A', 'E', 'I', 'O', 'U', 'Y') AND SUBSTRING(st, pos-1, 3) = 'ACH' AND
			   (SUBSTRING(st, pos+2, 1) NOT IN ('I', 'E') OR SUBSTRING(st, pos-2, 6) IN ('BACHER', 'MACHER'))) THEN
				SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 2; 
			
			ELSEIF pos = first AND SUBSTRING(st, first, 6) = 'CAESAR' THEN
				SET pri = CONCAT(pri, 'S'), sec = CONCAT(sec, 'S'), pos = pos  + 2; 
			ELSEIF SUBSTRING(st, pos, 4) = 'CHIA' THEN 
				SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 2; 
			ELSEIF SUBSTRING(st, pos, 2) = 'CH' THEN
				
				IF pos > first AND SUBSTRING(st, pos, 4) = 'CHAE' THEN
					SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'X'), pos = pos  + 2; 
				ELSEIF pos = first AND (SUBSTRING(st, pos+1, 5) IN ('HARAC', 'HARIS') OR
				   SUBSTRING(st, pos+1, 3) IN ('HOR', 'HYM', 'HIA', 'HEM')) AND SUBSTRING(st, first, 5) != 'CHORE' THEN
					SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 2; 
				
				ELSEIF SUBSTRING(st, first, 4) IN ('VAN ', 'VON ') OR SUBSTRING(st, first, 3) = 'SCH'
				   OR SUBSTRING(st, pos-2, 6) IN ('ORCHES', 'ARCHIT', 'ORCHID')
				   OR SUBSTRING(st, pos+2, 1) IN ('T', 'S')
				   OR ((SUBSTRING(st, pos-1, 1) IN ('A', 'O', 'U', 'E') OR pos = first)
				   AND SUBSTRING(st, pos+2, 1) IN ('L', 'R', 'N', 'M', 'B', 'H', 'F', 'V', 'W', ' ')) THEN
					SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 2; 
				ELSE
					IF pos > first THEN
						IF SUBSTRING(st, first, 2) = 'MC' THEN
							SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 2; 
						ELSE
							SET pri = CONCAT(pri, 'X'), sec = CONCAT(sec, 'K'), pos = pos  + 2; 
						END IF;
					ELSE
						SET pri = CONCAT(pri, 'X'), sec = CONCAT(sec, 'X'), pos = pos  + 2; 
					END IF;
				END IF;
			
			ELSEIF SUBSTRING(st, pos, 2) = 'CZ' AND SUBSTRING(st, pos-2, 4) != 'WICZ' THEN
				SET pri = CONCAT(pri, 'S'), sec = CONCAT(sec, 'X'), pos = pos  + 2; 
			
			ELSEIF SUBSTRING(st, pos+1, 3) = 'CIA' THEN
				SET pri = CONCAT(pri, 'X'), sec = CONCAT(sec, 'X'), pos = pos  + 3; 
			
			ELSEIF SUBSTRING(st, pos, 2) = 'CC' AND NOT (pos = (first +1) AND SUBSTRING(st, first, 1) = 'M') THEN
				
				IF SUBSTRING(st, pos+2, 1) IN ('I', 'E', 'H') AND SUBSTRING(st, pos+2, 2) != 'HU' THEN
					
					IF (pos = first +1 AND SUBSTRING(st, first) = 'A') OR
					   SUBSTRING(st, pos-1, 5) IN ('UCCEE', 'UCCES') THEN
						SET pri = CONCAT(pri, 'KS'), sec = CONCAT(sec, 'KS'), pos = pos  + 3; 
					
					ELSE
						SET pri = CONCAT(pri, 'X'), sec = CONCAT(sec, 'X'), pos = pos  + 3; 
					END IF;
				ELSE
					SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 2; 
				END IF;
			ELSEIF SUBSTRING(st, pos, 2) IN ('CK', 'CG', 'CQ') THEN
				SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 2; 
			ELSEIF SUBSTRING(st, pos, 2) IN ('CI', 'CE', 'CY') THEN
				
				IF SUBSTRING(st, pos, 3) IN ('CIO', 'CIE', 'CIA') THEN
					SET pri = CONCAT(pri, 'S'), sec = CONCAT(sec, 'X'), pos = pos  + 2; 
				ELSE
					SET pri = CONCAT(pri, 'S'), sec = CONCAT(sec, 'S'), pos = pos  + 2; 
				END IF;
			ELSE
				
				IF SUBSTRING(st, pos+1, 2) IN (' C', ' Q', ' G') THEN
					SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 3; 
				ELSE
					IF SUBSTRING(st, pos+1, 1) IN ('C', 'K', 'Q') AND SUBSTRING(st, pos+1, 2) NOT IN ('CE', 'CI') THEN
						SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 2; 
					ELSE 
						SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 1; 
					END IF;
				END IF;
			END IF;
		
			
		WHEN ch = 'D' THEN
			IF SUBSTRING(st, pos, 2) = 'DG' THEN
				IF SUBSTRING(st, pos+2, 1) IN ('I', 'E', 'Y') THEN 
					SET pri = CONCAT(pri, 'J'), sec = CONCAT(sec, 'J'), pos = pos  + 3; 
				ELSE
					SET pri = CONCAT(pri, 'TK'), sec = CONCAT(sec, 'TK'), pos = pos  + 2; 
				END IF;
			ELSEIF SUBSTRING(st, pos, 2) IN ('DT', 'DD') THEN
				SET pri = CONCAT(pri, 'T'), sec = CONCAT(sec, 'T'), pos = pos  + 2; 
			ELSE
				SET pri = CONCAT(pri, 'T'), sec = CONCAT(sec, 'T'), pos = pos  + 1; 
			END IF;
		WHEN ch = 'F' THEN
			IF SUBSTRING(st, pos+1, 1) = 'F' THEN
				SET pri = CONCAT(pri, 'F'), sec = CONCAT(sec, 'F'), pos = pos  + 2; 
			ELSE
				SET pri = CONCAT(pri, 'F'), sec = CONCAT(sec, 'F'), pos = pos  + 1; 
			END IF;
		WHEN ch = 'G' THEN
			IF SUBSTRING(st, pos+1, 1) = 'H' THEN
				IF (pos > first AND SUBSTRING(st, pos-1, 1) NOT IN ('A', 'E', 'I', 'O', 'U', 'Y'))
					OR ( pos = first AND SUBSTRING(st, pos+2, 1) != 'I') THEN
					SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 2; 
				ELSEIF pos = first AND SUBSTRING(st, pos+2, 1) = 'I' THEN
					 SET pri = CONCAT(pri, 'J'), sec = CONCAT(sec, 'J'), pos = pos  + 2; 
				
				ELSEIF (pos > (first + 1) AND SUBSTRING(st, pos-2, 1) IN ('B', 'H', 'D') )
				   OR (pos > (first + 2) AND SUBSTRING(st, pos-3, 1) IN ('B', 'H', 'D') )
				   OR (pos > (first + 3) AND SUBSTRING(st, pos-4, 1) IN ('B', 'H') ) THEN
					SET pos = pos + 2; 
				ELSE
					
					IF pos > (first + 2) AND SUBSTRING(st, pos-1, 1) = 'U'
					   AND SUBSTRING(st, pos-3, 1) IN ('C', 'G', 'L', 'R', 'T') THEN
						SET pri = CONCAT(pri, 'F'), sec = CONCAT(sec, 'F'), pos = pos  + 2; 
					ELSEIF pos > first AND SUBSTRING(st, pos-1, 1) != 'I' THEN
						SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 2; 
          ELSE
              SET pos = pos + 1;
					END IF;
				END IF;
			ELSEIF SUBSTRING(st, pos+1, 1) = 'N' THEN
				IF pos = (first +1) AND SUBSTRING(st, first, 1) IN ('A', 'E', 'I', 'O', 'U', 'Y') AND NOT is_slavo_germanic THEN
					SET pri = CONCAT(pri, 'KN'), sec = CONCAT(sec, 'N'), pos = pos  + 2; 
				ELSE
					
					IF SUBSTRING(st, pos+2, 2) != 'EY' AND SUBSTRING(st, pos+1, 1) != 'Y'
						AND NOT is_slavo_germanic THEN
						SET pri = CONCAT(pri, 'N'), sec = CONCAT(sec, 'KN'), pos = pos  + 2; 
					ELSE
						SET pri = CONCAT(pri, 'KN'), sec = CONCAT(sec, 'KN'), pos = pos  + 2; 
					END IF;
				END IF;
			
			ELSEIF SUBSTRING(st, pos+1, 2) = 'LI' AND NOT is_slavo_germanic THEN
				SET pri = CONCAT(pri, 'KL'), sec = CONCAT(sec, 'L'), pos = pos  + 2; 
			
			ELSEIF pos = first AND (SUBSTRING(st, pos+1, 1) = 'Y'
			   OR SUBSTRING(st, pos+1, 2) IN ('ES', 'EP', 'EB', 'EL', 'EY', 'IB', 'IL', 'IN', 'IE', 'EI', 'ER')) THEN
				SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'J'), pos = pos  + 2; 
			
			ELSEIF (SUBSTRING(st, pos+1, 2) = 'ER' OR SUBSTRING(st, pos+1, 1) = 'Y')
			   AND SUBSTRING(st, first, 6) NOT IN ('DANGER', 'RANGER', 'MANGER')
			   AND SUBSTRING(st, pos-1, 1) not IN ('E', 'I') AND SUBSTRING(st, pos-1, 3) NOT IN ('RGY', 'OGY') THEN
				SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'J'), pos = pos  + 2; 
			
			ELSEIF SUBSTRING(st, pos+1, 1) IN ('E', 'I', 'Y') OR SUBSTRING(st, pos-1, 4) IN ('AGGI', 'OGGI') THEN
				
				IF SUBSTRING(st, first, 4) IN ('VON ', 'VAN ') OR SUBSTRING(st, first, 3) = 'SCH'
				   OR SUBSTRING(st, pos+1, 2) = 'ET' THEN
					SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 2; 
				ELSE
					
					IF SUBSTRING(st, pos+1, 4) = 'IER ' THEN
						SET pri = CONCAT(pri, 'J'), sec = CONCAT(sec, 'J'), pos = pos  + 2; 
					ELSE
						SET pri = CONCAT(pri, 'J'), sec = CONCAT(sec, 'K'), pos = pos  + 2; 
					END IF;
				END IF;
			ELSEIF SUBSTRING(st, pos+1, 1) = 'G' THEN
				SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 2; 
			ELSE
				SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 1; 
			END IF;
		WHEN ch = 'H' THEN
			
			IF (pos = first OR SUBSTRING(st, pos-1, 1) IN ('A', 'E', 'I', 'O', 'U', 'Y'))
				AND SUBSTRING(st, pos+1, 1) IN ('A', 'E', 'I', 'O', 'U', 'Y') THEN
				SET pri = CONCAT(pri, 'H'), sec = CONCAT(sec, 'H'), pos = pos  + 2; 
			ELSE 
				SET pos = pos + 1; 
			END IF;
		WHEN ch = 'J' THEN
			
			IF SUBSTRING(st, pos, 4) = 'JOSE' OR SUBSTRING(st, first, 4) = 'SAN ' THEN
				IF (pos = first AND SUBSTRING(st, pos+4, 1) = ' ') OR SUBSTRING(st, first, 4) = 'SAN ' THEN
					SET pri = CONCAT(pri, 'H'), sec = CONCAT(sec, 'H'); 
				ELSE
					SET pri = CONCAT(pri, 'J'), sec = CONCAT(sec, 'H'); 
				END IF;
			ELSEIF pos = first AND SUBSTRING(st, pos, 4) != 'JOSE' THEN
				SET pri = CONCAT(pri, 'J'), sec = CONCAT(sec, 'A'); 
			ELSE
				
				IF SUBSTRING(st, pos-1, 1) IN ('A', 'E', 'I', 'O', 'U', 'Y') AND NOT is_slavo_germanic
				   AND SUBSTRING(st, pos+1, 1) IN ('A', 'O') THEN
					SET pri = CONCAT(pri, 'J'), sec = CONCAT(sec, 'H'); 
				ELSE
					IF pos = last THEN
						SET pri = CONCAT(pri, 'J'); 
					ELSE
						IF SUBSTRING(st, pos+1, 1) not IN ('L', 'T', 'K', 'S', 'N', 'M', 'B', 'Z')
						   AND SUBSTRING(st, pos-1, 1) not IN ('S', 'K', 'L') THEN
							SET pri = CONCAT(pri, 'J'), sec = CONCAT(sec, 'J'); 
						END IF;
					END IF;
				END IF;
			END IF;
			IF SUBSTRING(st, pos+1, 1) = 'J' THEN
				SET pos = pos + 2;
			ELSE
				SET pos = pos + 1;
			END IF;
		WHEN ch = 'K' THEN
			IF SUBSTRING(st, pos+1, 1) = 'K' THEN
				SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 2; 
			ELSE
				SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 1; 
			END IF;
		WHEN ch = 'L' THEN
			IF SUBSTRING(st, pos+1, 1) = 'L' THEN
				
				IF (pos = (last - 2) AND SUBSTRING(st, pos-1, 4) IN ('ILLO', 'ILLA', 'ALLE'))
				   OR ((SUBSTRING(st, last-1, 2) IN ('AS', 'OS') OR SUBSTRING(st, last) IN ('A', 'O'))
				   AND SUBSTRING(st, pos-1, 4) = 'ALLE') THEN
					SET pri = CONCAT(pri, 'L'), pos = pos  + 2; 
				ELSE
					SET pri = CONCAT(pri, 'L'), sec = CONCAT(sec, 'L'), pos = pos  + 2; 
				END IF;
			ELSE
				SET pri = CONCAT(pri, 'L'), sec = CONCAT(sec, 'L'), pos = pos  + 1; 
			END IF;
		WHEN ch = 'M' THEN
			IF SUBSTRING(st, pos-1, 3) = 'UMB'
			   AND (pos + 1 = last OR SUBSTRING(st, pos+2, 2) = 'ER')
			   OR SUBSTRING(st, pos+1, 1) = 'M' THEN
				SET pri = CONCAT(pri, 'M'), sec = CONCAT(sec, 'M'), pos = pos  + 2; 
			ELSE
				SET pri = CONCAT(pri, 'M'), sec = CONCAT(sec, 'M'), pos = pos  + 1; 
			END IF;
		WHEN ch = 'N' THEN
			IF SUBSTRING(st, pos+1, 1) = 'N' THEN
				SET pri = CONCAT(pri, 'N'), sec = CONCAT(sec, 'N'), pos = pos  + 2; 
			ELSE
				SET pri = CONCAT(pri, 'N'), sec = CONCAT(sec, 'N'), pos = pos  + 1; 
			END IF;
		
			
		WHEN ch = 'P' THEN
			IF SUBSTRING(st, pos+1, 1) = 'H' THEN
				SET pri = CONCAT(pri, 'F'), sec = CONCAT(sec, 'F'), pos = pos  + 2; 
			ELSEIF SUBSTRING(st, pos+1, 1) IN ('P', 'B') THEN 
				SET pri = CONCAT(pri, 'P'), sec = CONCAT(sec, 'P'), pos = pos  + 2; 
			ELSE
				SET pri = CONCAT(pri, 'P'), sec = CONCAT(sec, 'P'), pos = pos  + 1; 
			END IF;
		WHEN ch = 'Q' THEN
			IF SUBSTRING(st, pos+1, 1) = 'Q' THEN
				SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 2; 
			ELSE
				SET pri = CONCAT(pri, 'K'), sec = CONCAT(sec, 'K'), pos = pos  + 1; 
			END IF;
		WHEN ch = 'R' THEN
			
			IF pos = last AND not is_slavo_germanic
			   AND SUBSTRING(st, pos-2, 2) = 'IE' AND SUBSTRING(st, pos-4, 2) NOT IN ('ME', 'MA') THEN
				SET sec = CONCAT(sec, 'R'); 
			ELSE
				SET pri = CONCAT(pri, 'R'), sec = CONCAT(sec, 'R'); 
			END IF;
			IF SUBSTRING(st, pos+1, 1) = 'R' THEN
				SET pos = pos + 2;
			ELSE
				SET pos = pos + 1;
			END IF;
		WHEN ch = 'S' THEN
			
			IF SUBSTRING(st, pos-1, 3) IN ('ISL', 'YSL') THEN
				SET pos = pos + 1;
			
			ELSEIF pos = first AND SUBSTRING(st, first, 5) = 'SUGAR' THEN
				SET pri = CONCAT(pri, 'X'), sec = CONCAT(sec, 'S'), pos = pos  + 1; 
			ELSEIF SUBSTRING(st, pos, 2) = 'SH' THEN
				
				IF SUBSTRING(st, pos+1, 4) IN ('HEIM', 'HOEK', 'HOLM', 'HOLZ') THEN
					SET pri = CONCAT(pri, 'S'), sec = CONCAT(sec, 'S'), pos = pos  + 2; 
				ELSE
					SET pri = CONCAT(pri, 'X'), sec = CONCAT(sec, 'X'), pos = pos  + 2; 
				END IF;
			
			ELSEIF SUBSTRING(st, pos, 3) IN ('SIO', 'SIA') OR SUBSTRING(st, pos, 4) = 'SIAN' THEN
				IF NOT is_slavo_germanic THEN
					SET pri = CONCAT(pri, 'S'), sec = CONCAT(sec, 'X'), pos = pos  + 3; 
				ELSE
					SET pri = CONCAT(pri, 'S'), sec = CONCAT(sec, 'S'), pos = pos  + 3; 
				END IF;
			
			
			ELSEIF (pos = first AND SUBSTRING(st, pos+1, 1) IN ('M', 'N', 'L', 'W')) OR SUBSTRING(st, pos+1, 1) = 'Z' THEN
				SET pri = CONCAT(pri, 'S'), sec = CONCAT(sec, 'X'); 
				IF SUBSTRING(st, pos+1, 1) = 'Z' THEN
					SET pos = pos + 2;
				ELSE
					SET pos = pos + 1;
				END IF;
			ELSEIF SUBSTRING(st, pos, 2) = 'SC' THEN
				
				IF SUBSTRING(st, pos+2, 1) = 'H' THEN
					
					IF SUBSTRING(st, pos+3, 2) IN ('OO', 'ER', 'EN', 'UY', 'ED', 'EM') THEN
						
						IF SUBSTRING(st, pos+3, 2) IN ('ER', 'EN') THEN
							SET pri = CONCAT(pri, 'X'), sec = CONCAT(sec, 'SK'), pos = pos  + 3; 
						ELSE
							SET pri = CONCAT(pri, 'SK'), sec = CONCAT(sec, 'SK'), pos = pos  + 3; 
						END IF;
					ELSE
						IF pos = first AND SUBSTRING(st, first+3, 1) not IN ('A', 'E', 'I', 'O', 'U', 'Y') AND SUBSTRING(st, first+3, 1) != 'W' THEN
							SET pri = CONCAT(pri, 'X'), sec = CONCAT(sec, 'S'), pos = pos  + 3; 
						ELSE
							SET pri = CONCAT(pri, 'X'), sec = CONCAT(sec, 'X'), pos = pos  + 3; 
						END IF;
					END IF;
				ELSEIF SUBSTRING(st, pos+2, 1) IN ('I', 'E', 'Y') THEN
					SET pri = CONCAT(pri, 'S'), sec = CONCAT(sec, 'S'), pos = pos  + 3; 
				ELSE
					SET pri = CONCAT(pri, 'SK'), sec = CONCAT(sec, 'SK'), pos = pos  + 3; 
				END IF;
			
			ELSEIF pos = last AND SUBSTRING(st, pos-2, 2) IN ('AI', 'OI') THEN
				SET sec = CONCAT(sec, 'S'), pos = pos  + 1; 
			ELSE
				SET pri = CONCAT(pri, 'S'), sec = CONCAT(sec, 'S'); 
				IF SUBSTRING(st, pos+1, 1) IN ('S', 'Z') THEN
					SET pos = pos + 2;
				ELSE
					SET pos = pos + 1;
				END IF;
			END IF;
		WHEN ch = 'T' THEN
			IF SUBSTRING(st, pos, 4) = 'TION' THEN
				SET pri = CONCAT(pri, 'X'), sec = CONCAT(sec, 'X'), pos = pos  + 3; 
			ELSEIF SUBSTRING(st, pos, 3) IN ('TIA', 'TCH') THEN
				SET pri = CONCAT(pri, 'X'), sec = CONCAT(sec, 'X'), pos = pos  + 3; 
			ELSEIF SUBSTRING(st, pos, 2) = 'TH' OR SUBSTRING(st, pos, 3) = 'TTH' THEN
				
				IF SUBSTRING(st, pos+2, 2) IN ('OM', 'AM') OR SUBSTRING(st, first, 4) IN ('VON ', 'VAN ')
				   OR SUBSTRING(st, first, 3) = 'SCH' THEN
					SET pri = CONCAT(pri, 'T'), sec = CONCAT(sec, 'T'), pos = pos  + 2; 
				ELSE
					SET pri = CONCAT(pri, '0'), sec = CONCAT(sec, 'T'), pos = pos  + 2; 
				END IF;
			ELSEIF SUBSTRING(st, pos+1, 1) IN ('T', 'D') THEN
				SET pri = CONCAT(pri, 'T'), sec = CONCAT(sec, 'T'), pos = pos  + 2; 
			ELSE
				SET pri = CONCAT(pri, 'T'), sec = CONCAT(sec, 'T'), pos = pos  + 1; 
			END IF;
		WHEN ch = 'V' THEN
			IF SUBSTRING(st, pos+1, 1) = 'V' THEN
				SET pri = CONCAT(pri, 'F'), sec = CONCAT(sec, 'F'), pos = pos  + 2; 
			ELSE
				SET pri = CONCAT(pri, 'F'), sec = CONCAT(sec, 'F'), pos = pos  + 1; 
			END IF;
		WHEN ch = 'W' THEN
			
			IF SUBSTRING(st, pos, 2) = 'WR' THEN
				SET pri = CONCAT(pri, 'R'), sec = CONCAT(sec, 'R'), pos = pos  + 2; 
			ELSEIF pos = first AND (SUBSTRING(st, pos+1, 1) IN ('A', 'E', 'I', 'O', 'U', 'Y')
				OR SUBSTRING(st, pos, 2) = 'WH') THEN
				
				IF SUBSTRING(st, pos+1, 1) IN ('A', 'E', 'I', 'O', 'U', 'Y') THEN
					SET pri = CONCAT(pri, 'A'), sec = CONCAT(sec, 'F'), pos = pos  + 1; 
				ELSE
					SET pri = CONCAT(pri, 'A'), sec = CONCAT(sec, 'A'), pos = pos  + 1; 
				END IF;
			
			ELSEIF (pos = last AND SUBSTRING(st, pos-1, 1) IN ('A', 'E', 'I', 'O', 'U', 'Y'))
			   OR SUBSTRING(st, pos-1, 5) IN ('EWSKI', 'EWSKY', 'OWSKI', 'OWSKY')
			   OR SUBSTRING(st, first, 3) = 'SCH' THEN
				SET sec = CONCAT(sec, 'F'), pos = pos  + 1; 
			
			
			ELSEIF SUBSTRING(st, pos, 4) IN ('WICZ', 'WITZ') THEN
				SET pri = CONCAT(pri, 'TS'), sec = CONCAT(sec, 'FX'), pos = pos  + 4; 
			ELSE 
				SET pos = pos + 1;
			END IF;
		WHEN ch = 'X' THEN
			
			IF not(pos = last AND (SUBSTRING(st, pos-3, 3) IN ('IAU', 'EAU')
			   OR SUBSTRING(st, pos-2, 2) IN ('AU', 'OU'))) THEN
				SET pri = CONCAT(pri, 'KS'), sec = CONCAT(sec, 'KS'); 
			END IF;
			IF SUBSTRING(st, pos+1, 1) IN ('C', 'X') THEN
				SET pos = pos + 2;
			ELSE
				SET pos = pos + 1;
			END IF;
		WHEN ch = 'Z' THEN
			
			IF SUBSTRING(st, pos+1, 1) = 'H' THEN
				SET pri = CONCAT(pri, 'J'), sec = CONCAT(sec, 'J'), pos = pos  + 1; 
			ELSEIF SUBSTRING(st, pos+1, 3) IN ('ZO', 'ZI', 'ZA')
			   OR (is_slavo_germanic AND pos > first AND SUBSTRING(st, pos-1, 1) != 'T') THEN
				SET pri = CONCAT(pri, 'S'), sec = CONCAT(sec, 'TS'); 
			ELSE
				SET pri = CONCAT(pri, 'S'), sec = CONCAT(sec, 'S'); 
			END IF;
			IF SUBSTRING(st, pos+1, 1) = 'Z' THEN
				SET pos = pos + 2;
			ELSE
				SET pos = pos + 1;
			END IF;
		ELSE
			SET pos = pos + 1; 
		END CASE;
    IF pos = prevpos THEN
       SET pos = pos +1;
       SET pri = CONCAT(pri,'<didnt incr>'); 
    END IF;
	END WHILE;
	IF pri != sec THEN
		SET pri = sec;
  END IF;
	RETURN (pri);
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `appel`
--

DROP TABLE IF EXISTS `appel`;
CREATE TABLE IF NOT EXISTS `appel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `motif` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `dateHeure` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `appel`
--

INSERT INTO `appel` (`id`, `motif`, `description`, `dateHeure`) VALUES
(1, 'Renseignement', 'test', '2019-05-28 00:00:00'),
(2, 'Renseignement', 'test', '2019-05-28 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `id_Praticien` int(11) NOT NULL,
  PRIMARY KEY (`id_Praticien`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`id_Praticien`) VALUES
(3),
(4),
(7),
(9),
(10),
(11),
(12),
(13),
(17),
(20),
(21),
(24),
(25),
(29),
(32),
(33),
(34),
(35),
(37),
(41),
(42),
(43),
(44),
(45),
(46),
(50),
(51);

--
-- Déclencheurs `client`
--
DROP TRIGGER IF EXISTS `client_ai`;
DELIMITER $$
CREATE TRIGGER `client_ai` AFTER INSERT ON `client` FOR EACH ROW DELETE FROM prospect WHERE id_Praticien = NEW.id_Praticien
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `composant`
--

DROP TABLE IF EXISTS `composant`;
CREATE TABLE IF NOT EXISTS `composant` (
  `CMP_CODE` varchar(4) NOT NULL,
  `CMP_LIBELLE` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`CMP_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `constituer`
--

DROP TABLE IF EXISTS `constituer`;
CREATE TABLE IF NOT EXISTS `constituer` (
  `MED_DEPOTLEGAL` varchar(10) NOT NULL,
  `CMP_CODE` varchar(4) NOT NULL,
  `CST_QTE` float DEFAULT NULL,
  PRIMARY KEY (`MED_DEPOTLEGAL`,`CMP_CODE`),
  KEY `MED_DEPOTLEGAL` (`MED_DEPOTLEGAL`),
  KEY `CMP_CODE` (`CMP_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `departement`
--

DROP TABLE IF EXISTS `departement`;
CREATE TABLE IF NOT EXISTS `departement` (
  `id` int(11) NOT NULL,
  `code` varchar(3) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `nom_reel` varchar(255) DEFAULT NULL,
  `nom_soundex` varchar(20) DEFAULT NULL,
  `nom_metaphone` varchar(22) DEFAULT NULL,
  `id_region` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `departement`
--

INSERT INTO `departement` (`id`, `code`, `slug`, `nom`, `nom_reel`, `nom_soundex`, `nom_metaphone`, `id_region`) VALUES
(1, '01', 'ain', 'AIN', 'Ain', 'A500', 'AN', 3),
(2, '02', 'aisne', 'AISNE', 'Aisne', 'A250', 'ASN', 9),
(3, '03', 'allier', 'ALLIER', 'Allier', 'A460', 'ALR', 3),
(4, '04', 'alpes-de-haute-provence', 'ALPES-DE-HAUTE-PROVENCE', 'Alpes-de-Haute-Provence', 'A412316152', 'ALPSTTPRFNS', 12),
(5, '05', 'hautes-alpes', 'HAUTES-ALPES', 'Hautes-Alpes', 'H32412', 'HTSLPS', 12),
(6, '06', 'alpes-maritimes', 'ALPES-MARITIMES', 'Alpes-Maritimes', 'A41256352', 'ALPSMRTMS', 12),
(7, '07', 'ardeche', 'ARDÈCHE', 'Ardèche', 'A632', 'ARTK', 3),
(8, '08', 'ardennes', 'ARDENNES', 'Ardennes', 'A6352', 'ARTNS', 1),
(9, '09', 'ariege', 'ARIÈGE', 'Ariège', 'A620', 'ARK', 8),
(10, '10', 'aube', 'AUBE', 'Aube', 'A100', 'AP', 1),
(11, '11', 'aude', 'AUDE', 'Aude', 'A300', 'AT', 8),
(12, '12', 'aveyron', 'AVEYRON', 'Aveyron', 'A165', 'AFRN', 8),
(13, '13', 'bouches-du-rhone', 'BOUCHES-DU-RHÔNE', 'Bouches-du-Rhône', 'B2365', 'PKSTRN', 12),
(14, '14', 'calvados', 'CALVADOS', 'Calvados', 'C4132', 'KLFTS', 10),
(15, '15', 'cantal', 'CANTAL', 'Cantal', 'C534', 'KNTL', 3),
(16, '16', 'charente', 'CHARENTE', 'Charente', 'C653', 'XRNT', 2),
(17, '17', 'charente-maritime', 'CHARENTE-MARITIME', 'Charente-Maritime', 'C6535635', 'XRNTMRTM', 2),
(18, '18', 'cher', 'CHER', 'Cher', 'C600', 'XR', 6),
(19, '19', 'correze', 'CORRÈZE', 'Corrèze', 'C620', 'KRS', 2),
(20, '2a', 'corse-du-sud', 'CORSE-DU-SUD', 'Corse-du-sud', 'C62323', 'KRSTST', 13),
(21, '2b', 'haute-corse', 'HAUTE-CORSE', 'Haute-corse', 'H3262', 'HTKRS', 13),
(22, '21', 'cote-dor', 'CÔTE-D\'OR', 'Côte-d\'or', 'C360', 'KTTR', 4),
(23, '22', 'cotes-darmor', 'CÔTES-D\'ARMOR', 'Côtes-d\'armor', 'C323656', 'KTSTRMR', 5),
(24, '23', 'creuse', 'CREUSE', 'Creuse', 'C620', 'KRS', 2),
(25, '24', 'dordogne', 'DORDOGNE', 'Dordogne', 'D6325', 'TRTKN', 2),
(26, '25', 'doubs', 'DOUBS', 'Doubs', 'D120', 'TPS', 4),
(27, '26', 'drome', 'DRÔME', 'Drôme', 'D650', 'TRM', 3),
(28, '27', 'eure', 'EURE', 'Eure', 'E600', 'ER', 10),
(29, '28', 'eure-et-loir', 'EURE-ET-LOIR', 'Eure-et-Loir', 'E6346', 'ERTLR', 6),
(30, '29', 'finistere', 'FINISTÈRE', 'Finistère', 'F5236', 'FNSTR', 5),
(31, '30', 'gard', 'GARD', 'Gard', 'G630', 'KRT', 8),
(32, '31', 'haute-garonne', 'HAUTE-GARONNE', 'Haute-Garonne', 'H3265', 'HTKRN', 8),
(33, '32', 'gers', 'GERS', 'Gers', 'G620', 'JRS', 8),
(34, '33', 'gironde', 'GIRONDE', 'Gironde', 'G653', 'JRNT', 2),
(35, '34', 'herault', 'HÉRAULT', 'Hérault', 'H643', 'HRLT', 8),
(36, '35', 'ile-et-vilaine', 'ILE-ET-VILAINE', 'Ile-et-Vilaine', 'I43145', 'ILTFLN', 5),
(37, '36', 'indre', 'INDRE', 'Indre', 'I536', 'INTR', 6),
(38, '37', 'indre-et-loire', 'INDRE-ET-LOIRE', 'Indre-et-Loire', 'I536346', 'INTRTLR', 6),
(39, '38', 'isere', 'ISÈRE', 'Isère', 'I260', 'ISKR', 3),
(40, '39', 'jura', 'JURA', 'Jura', 'J600', 'JR', 4),
(41, '40', 'landes', 'LANDES', 'Landes', 'L532', 'LNTS', 2),
(42, '41', 'loir-et-cher', 'LOIR-ET-CHER', 'Loir-et-Cher', 'L6326', 'LRTKR', 6),
(43, '42', 'loire', 'LOIRE', 'Loire', 'L600', 'LR', 3),
(44, '43', 'haute-loire', 'HAUTE-LOIRE', 'Haute-Loire', 'H346', 'HTLR', 3),
(45, '44', 'loire-atlantique', 'LOIRE-ATLANTIQUE', 'Loire-Atlantique', 'L634532', 'LRTLNTK', 11),
(46, '45', 'loiret', 'LOIRET', 'Loiret', 'L630', 'LRT', 6),
(47, '46', 'lot', 'LOT', 'Lot', 'L300', 'LT', 8),
(48, '47', 'lot-et-garonne', 'LOT-ET-GARONNE', 'Lot-et-Garonne', 'L3265', 'LTTKRN', 2),
(49, '48', 'lozere', 'LOZÈRE', 'Lozère', 'L260', 'LSR', 8),
(50, '49', 'maine-et-loire', 'MAINE-ET-LOIRE', 'Maine-et-Loire', 'M346', 'MNTLR', 11),
(51, '50', 'manche', 'MANCHE', 'Manche', 'M200', 'MNK', 10),
(52, '51', 'marne', 'MARNE', 'Marne', 'M650', 'MRN', 1),
(53, '52', 'haute-marne', 'HAUTE-MARNE', 'Haute-Marne', 'H3565', 'HTMRN', 1),
(54, '53', 'mayenne', 'MAYENNE', 'Mayenne', 'M000', 'MN', 11),
(55, '54', 'meurthe-et-moselle', 'MEURTHE-ET-MOSELLE', 'Meurthe-et-Moselle', 'M63524', 'MRTTMSL', 1),
(56, '55', 'meuse', 'MEUSE', 'Meuse', 'M200', 'MS', 1),
(57, '56', 'morbihan', 'MORBIHAN', 'Morbihan', 'M615', 'MRPHN', 5),
(58, '57', 'moselle', 'MOSELLE', 'Moselle', 'M240', 'MSL', 1),
(59, '58', 'nievre', 'NIÈVRE', 'Nièvre', 'N160', 'NFR', 4),
(60, '59', 'nord', 'NORD', 'Nord', 'N630', 'NRT', 9),
(61, '60', 'oise', 'OISE', 'Oise', 'O200', 'OS', 9),
(62, '61', 'orne', 'ORNE', 'Orne', 'O650', 'ORN', 10),
(63, '62', 'pas-de-calais', 'PAS-DE-CALAIS', 'Pas-de-Calais', 'P23242', 'PSTKLS', 9),
(64, '63', 'puy-de-dome', 'PUY-DE-DÔME', 'Puy-de-Dôme', 'P350', 'PTTM', 3),
(65, '64', 'pyrenees-atlantiques', 'PYRÉNÉES-ATLANTIQUES', 'Pyrénées-Atlantiques', 'P65234532', 'PRNSTLNTKS', 2),
(66, '65', 'hautes-pyrenees', 'HAUTES-PYRÉNÉES', 'Hautes-Pyrénées', 'H321652', 'HTSPRNS', 8),
(67, '66', 'pyrenees-orientales', 'PYRÉNÉES-ORIENTALES', 'Pyrénées-Orientales', 'P65265342', 'PRNSRNTLS', 8),
(68, '67', 'bas-rhin', 'BAS-RHIN', 'Bas-Rhin', 'B265', 'BSRN', 1),
(69, '68', 'haut-rhin', 'HAUT-RHIN', 'Haut-Rhin', 'H365', 'HTRN', 1),
(70, '69', 'rhone', 'RHÔNE', 'Rhône', 'R500', 'RN', 3),
(71, '70', 'haute-saone', 'HAUTE-SAÔNE', 'Haute-Saône', 'H325', 'HTSN', 4),
(72, '71', 'saone-et-loire', 'SAÔNE-ET-LOIRE', 'Saône-et-Loire', 'S5346', 'SNTLR', 4),
(73, '72', 'sarthe', 'SARTHE', 'Sarthe', 'S630', 'SRT', 11),
(74, '73', 'savoie', 'SAVOIE', 'Savoie', 'S100', 'SF', 3),
(75, '74', 'haute-savoie', 'HAUTE-SAVOIE', 'Haute-Savoie', 'H321', 'HTSF', 3),
(76, '75', 'paris', 'PARIS', 'Paris', 'P620', 'PRS', 7),
(77, '76', 'seine-maritime', 'SEINE-MARITIME', 'Seine-Maritime', 'S5635', 'SNMRTM', 10),
(78, '77', 'seine-et-marne', 'SEINE-ET-MARNE', 'Seine-et-Marne', 'S53565', 'SNTMRN', 7),
(79, '78', 'yvelines', 'YVELINES', 'Yvelines', 'Y1452', 'FLNS', 7),
(80, '79', 'deux-sevres', 'DEUX-SÈVRES', 'Deux-Sèvres', 'D2162', 'TKSSFRS', 2),
(81, '80', 'somme', 'SOMME', 'Somme', 'S500', 'SM', 9),
(82, '81', 'tarn', 'TARN', 'Tarn', 'T650', 'TRN', 8),
(83, '82', 'tarn-et-garonne', 'TARN-ET-GARONNE', 'Tarn-et-Garonne', 'T653265', 'TRNTKRN', 8),
(84, '83', 'var', 'VAR', 'Var', 'V600', 'FR', 12),
(85, '84', 'vaucluse', 'VAUCLUSE', 'Vaucluse', 'V242', 'FKLS', 12),
(86, '85', 'vendee', 'VENDÉE', 'Vendée', 'V530', 'FNT', 11),
(87, '86', 'vienne', 'VIENNE', 'Vienne', 'V500', 'FN', 2),
(88, '87', 'haute-vienne', 'HAUTE-VIENNE', 'Haute-Vienne', 'H315', 'HTFN', 2),
(89, '88', 'vosges', 'VOSGES', 'Vosges', 'V200', 'FSKS', 1),
(90, '89', 'yonne', 'YONNE', 'Yonne', 'Y500', 'YN', 4),
(91, '90', 'territoire-de-belfort', 'TERRITOIRE DE BELFORT', 'Territoire de Belfort', 'T636314163', 'TRTRTPLFRT', 4),
(92, '91', 'essonne', 'ESSONNE', 'Essonne', 'E250', 'ASN', 7),
(93, '92', 'hauts-de-seine', 'HAUTS-DE-SEINE', 'Hauts-de-Seine', 'H32325', 'HTSTSN', 7),
(94, '93', 'seine-saint-denis', 'SEINE-SAINT-DENIS', 'Seine-Saint-Denis', 'S525352', 'SNSNTTNS', 7),
(95, '94', 'val-de-marne', 'VAL-DE-MARNE', 'Val-de-Marne', 'V43565', 'FLTMRN', 7),
(96, '95', 'val-doise', 'VAL-D\'OISE', 'Val-d\'oise', 'V432', 'FLTS', 7),
(97, '976', 'mayotte', 'MAYOTTE', 'Mayotte', 'M300', 'MT', 18),
(98, '971', 'guadeloupe', 'GUADELOUPE', 'Guadeloupe', 'G341', 'KTLP', 17),
(99, '973', 'guyane', 'GUYANE', 'Guyane', 'G500', 'KN', 16),
(100, '972', 'martinique', 'MARTINIQUE', 'Martinique', 'M6352', 'MRTNK', 14),
(101, '974', 'reunion', 'RÉUNION', 'Réunion', 'R500', 'RNN', 15),
(102, '975', 'Saint-Pierre-et-Miquelon', 'SAINT-PIERRE-ET-MIQUELON', 'Saint-Pierre-et-Miquelon', 'S531635245', 'SNTPRTMKLN', 19);

-- --------------------------------------------------------

--
-- Structure de la table `dosage`
--

DROP TABLE IF EXISTS `dosage`;
CREATE TABLE IF NOT EXISTS `dosage` (
  `DOS_CODE` varchar(10) NOT NULL,
  `DOS_QUANTITE` varchar(10) DEFAULT NULL,
  `DOS_UNITE` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`DOS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `droit`
--

DROP TABLE IF EXISTS `droit`;
CREATE TABLE IF NOT EXISTS `droit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `etat`
--

DROP TABLE IF EXISTS `etat`;
CREATE TABLE IF NOT EXISTS `etat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `etat`
--

INSERT INTO `etat` (`id`, `nom`) VALUES
(1, 'nouveau'),
(2, 'à rappeler'),
(3, 'rendez-vous en attente'),
(4, 'rendez-vous à confirmer'),
(5, 'rendez-vous confirmé');

-- --------------------------------------------------------

--
-- Structure de la table `famille`
--

DROP TABLE IF EXISTS `famille`;
CREATE TABLE IF NOT EXISTS `famille` (
  `FAM_CODE` varchar(3) NOT NULL,
  `FAM_LIBELLE` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`FAM_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `famille`
--

INSERT INTO `famille` (`FAM_CODE`, `FAM_LIBELLE`) VALUES
('AA', 'Antalgiques en association'),
('AAA', 'Antalgiques antipyrétiques en association'),
('AAC', 'Antidépresseur d\'action centrale'),
('AAH', 'Antivertigineux antihistaminique H1'),
('ABA', 'Antibiotique antituberculeux'),
('ABC', 'Antibiotique antiacnéique local'),
('ABP', 'Antibiotique de la famille des béta-lactamines (pénicilline A)'),
('AFC', 'Antibiotique de la famille des cyclines'),
('AFM', 'Antibiotique de la famille des macrolides'),
('AH', 'Antihistaminique H1 local'),
('AIM', 'Antidépresseur imipraminique (tricyclique)'),
('AIN', 'Antidépresseur inhibiteur sélectif de la recapture de la sérotonine'),
('ALO', 'Antibiotique local (ORL)'),
('ANS', 'Antidépresseur IMAO non sélectif'),
('AO', 'Antibiotique ophtalmique'),
('AP', 'Antipsychotique normothymique'),
('AUM', 'Antibiotique urinaire minute'),
('CRT', 'Corticoïde, antibiotique et antifongique à  usage local'),
('HYP', 'Hypnotique antihistaminique'),
('PSA', 'Psychostimulant, antiasthénique');

-- --------------------------------------------------------

--
-- Structure de la table `formuler`
--

DROP TABLE IF EXISTS `formuler`;
CREATE TABLE IF NOT EXISTS `formuler` (
  `MED_DEPOTLEGAL` varchar(10) NOT NULL,
  `PRE_CODE` varchar(2) NOT NULL,
  PRIMARY KEY (`MED_DEPOTLEGAL`,`PRE_CODE`),
  KEY `MED_DEPOTLEGAL` (`MED_DEPOTLEGAL`),
  KEY `PRE_CODE` (`PRE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `groupe`
--

DROP TABLE IF EXISTS `groupe`;
CREATE TABLE IF NOT EXISTS `groupe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `groupe`
--

INSERT INTO `groupe` (`id`, `nom`) VALUES
(1, 'Visiteur(s)'),
(2, 'Responsable(s) d\'Agence'),
(3, 'Responsable(s) Régional'),
(4, 'Administrateur(s)');

-- --------------------------------------------------------

--
-- Structure de la table `groupe_droit`
--

DROP TABLE IF EXISTS `groupe_droit`;
CREATE TABLE IF NOT EXISTS `groupe_droit` (
  `id_groupe` int(11) NOT NULL,
  `id_droit` int(11) NOT NULL,
  PRIMARY KEY (`id_groupe`,`id_droit`),
  KEY `id_droit` (`id_droit`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `interagir`
--

DROP TABLE IF EXISTS `interagir`;
CREATE TABLE IF NOT EXISTS `interagir` (
  `MED_PERTURBATEUR` varchar(10) NOT NULL,
  `MED_MED_PERTURBE` varchar(10) NOT NULL,
  PRIMARY KEY (`MED_PERTURBATEUR`,`MED_MED_PERTURBE`),
  KEY `MED_MED_PERTURBE` (`MED_MED_PERTURBE`),
  KEY `MED_PERTURBATEUR` (`MED_PERTURBATEUR`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `interesser`
--

DROP TABLE IF EXISTS `interesser`;
CREATE TABLE IF NOT EXISTS `interesser` (
  `id_Client` int(11) NOT NULL,
  `id_Prestation` int(11) NOT NULL,
  PRIMARY KEY (`id_Prestation`,`id_Client`),
  KEY `id_Client` (`id_Client`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `interesser`
--

INSERT INTO `interesser` (`id_Client`, `id_Prestation`) VALUES
(3, 3),
(4, 1),
(4, 2),
(7, 2),
(7, 3),
(9, 1),
(9, 2),
(9, 3),
(10, 1),
(11, 1),
(12, 2),
(12, 3),
(13, 1),
(17, 1),
(20, 2),
(21, 2),
(24, 3),
(25, 1),
(25, 2),
(25, 3),
(29, 1),
(29, 2),
(29, 3),
(32, 1),
(32, 2),
(33, 1),
(34, 2),
(35, 3),
(37, 1),
(37, 2),
(41, 1),
(42, 1),
(43, 1),
(43, 2),
(44, 2),
(45, 3),
(46, 1),
(50, 1),
(51, 1),
(51, 2);

-- --------------------------------------------------------

--
-- Structure de la table `medicament`
--

DROP TABLE IF EXISTS `medicament`;
CREATE TABLE IF NOT EXISTS `medicament` (
  `MED_DEPOTLEGAL` varchar(10) NOT NULL,
  `MED_NOMCOMMERCIAL` varchar(25) DEFAULT NULL,
  `FAM_CODE` varchar(3) NOT NULL,
  `MED_COMPOSITION` varchar(255) DEFAULT NULL,
  `MED_EFFETS` varchar(255) DEFAULT NULL,
  `MED_CONTREINDIC` varchar(255) DEFAULT NULL,
  `MED_PRIXECHANTILLON` float DEFAULT NULL,
  PRIMARY KEY (`MED_DEPOTLEGAL`),
  KEY `FAM_CODE` (`FAM_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `medicament`
--

INSERT INTO `medicament` (`MED_DEPOTLEGAL`, `MED_NOMCOMMERCIAL`, `FAM_CODE`, `MED_COMPOSITION`, `MED_EFFETS`, `MED_CONTREINDIC`, `MED_PRIXECHANTILLON`) VALUES
('3MYC7', 'TRIMYCINE', 'CRT', 'Triamcinolone (acétonide) + Néomycine + Nystatine', 'Ce médicament est un corticoïde à  activité forte ou très forte associé à  un antibiotique et un antifongique, utilisé en application locale dans certaines atteintes cutanées surinfectées.', 'Ce médicament est contre-indiqué en cas d\'allergie à  l\'un des constituants, d\'infections de la peau ou de parasitisme non traités, d\'acné. Ne pas appliquer sur une plaie, ni sous un pansement occlusif.', NULL),
('ADIMOL9', 'ADIMOL', 'ABP', 'Amoxicilline + Acide clavulanique', 'Ce médicament, plus puissant que les pénicillines simples, est utilisé pour traiter des infections bactériennes spécifiques.', 'Ce médicament est contre-indiqué en cas d\'allergie aux pénicillines ou aux céphalosporines.', NULL),
('AMOPIL7', 'AMOPIL', 'ABP', 'Amoxicilline', 'Ce médicament, plus puissant que les pénicillines simples, est utilisé pour traiter des infections bactériennes spécifiques.', 'Ce médicament est contre-indiqué en cas d\'allergie aux pénicillines. Il doit être administré avec prudence en cas d\'allergie aux céphalosporines.', NULL),
('AMOX45', 'AMOXAR', 'ABP', 'Amoxicilline', 'Ce médicament, plus puissant que les pénicillines simples, est utilisé pour traiter des infections bactériennes spécifiques.', 'La prise de ce médicament peut rendre positifs les tests de dépistage du dopage.', NULL),
('AMOXIG12', 'AMOXI Gé', 'ABP', 'Amoxicilline', 'Ce médicament, plus puissant que les pénicillines simples, est utilisé pour traiter des infections bactériennes spécifiques.', 'Ce médicament est contre-indiqué en cas d\'allergie aux pénicillines. Il doit être administré avec prudence en cas d\'allergie aux céphalosporines.', NULL),
('APATOUX22', 'APATOUX Vitamine C', 'ALO', 'Tyrothricine + Tétracaïne + Acide ascorbique (Vitamine C)', 'Ce médicament est utilisé pour traiter les affections de la bouche et de la gorge.', 'Ce médicament est contre-indiqué en cas d\'allergie à  l\'un des constituants, en cas de phénylcétonurie et chez l\'enfant de moins de 6 ans.', NULL),
('BACTIG10', 'BACTIGEL', 'ABC', 'Erythromycine', 'Ce médicament est utilisé en application locale pour traiter l\'acné et les infections cutanées bactériennes associées.', 'Ce médicament est contre-indiqué en cas d\'allergie aux antibiotiques de la famille des macrolides ou des lincosanides.', NULL),
('BACTIV13', 'BACTIVIL', 'AFM', 'Erythromycine', 'Ce médicament est utilisé pour traiter des infections bactériennes spécifiques.', 'Ce médicament est contre-indiqué en cas d\'allergie aux macrolides (dont le chef de file est l\'érythromycine).', NULL),
('BITALV', 'BIVALIC', 'AAA', 'Dextropropoxyphène + Paracétamol', 'Ce médicament est utilisé pour traiter les douleurs d\'intensité modérée ou intense.', 'Ce médicament est contre-indiqué en cas d\'allergie aux médicaments de cette famille, d\'insuffisance hépatique ou d\'insuffisance rénale.', NULL),
('CARTION6', 'CARTION', 'AAA', 'Acide acétylsalicylique (aspirine) + Acide ascorbique (Vitamine C) + Paracétamol', 'Ce médicament est utilisé dans le traitement symptomatique de la douleur ou de la fièvre.', 'Ce médicament est contre-indiqué en cas de troubles de la coagulation (tendances aux hémorragies), d\'ulcère gastroduodénal, maladies graves du foie.', NULL),
('CLAZER6', 'CLAZER', 'AFM', 'Clarithromycine', 'Ce médicament est utilisé pour traiter des infections bactériennes spécifiques. Il est également utilisé dans le traitement de l\'ulcère gastro-duodénal, en association avec d\'autres médicaments.', 'Ce médicament est contre-indiqué en cas d\'allergie aux macrolides (dont le chef de file est l\'érythromycine).', NULL),
('DEPRIL9', 'DEPRAMIL', 'AIM', 'Clomipramine', 'Ce médicament est utilisé pour traiter les épisodes dépressifs sévères, certaines douleurs rebelles, les troubles obsessionnels compulsifs et certaines énurésies chez l\'enfant.', 'Ce médicament est contre-indiqué en cas de glaucome ou d\'adénome de la prostate, d\'infarctus récent, ou si vous avez reà§u un traitement par IMAO durant les 2 semaines précédentes ou en cas d\'allergie aux antidépresseurs imipraminiques.', NULL),
('DIMIRTAM6', 'DIMIRTAM', 'AAC', 'Mirtazapine', 'Ce médicament est utilisé pour traiter les épisodes dépressifs sévères.', 'La prise de ce produit est contre-indiquée en cas de d\'allergie à  l\'un des constituants.', NULL),
('DOLRIL7', 'DOLORIL', 'AAA', 'Acide acétylsalicylique (aspirine) + Acide ascorbique (Vitamine C) + Paracétamol', 'Ce médicament est utilisé dans le traitement symptomatique de la douleur ou de la fièvre.', 'Ce médicament est contre-indiqué en cas d\'allergie au paracétamol ou aux salicylates.', NULL),
('DORNOM8', 'NORMADOR', 'HYP', 'Doxylamine', 'Ce médicament est utilisé pour traiter l\'insomnie chez l\'adulte.', 'Ce médicament est contre-indiqué en cas de glaucome, de certains troubles urinaires (rétention urinaire) et chez l\'enfant de moins de 15 ans.', NULL),
('EQUILARX6', 'EQUILAR', 'AAH', 'Méclozine', 'Ce médicament est utilisé pour traiter les vertiges et pour prévenir le mal des transports.', 'Ce médicament ne doit pas être utilisé en cas d\'allergie au produit, en cas de glaucome ou de rétention urinaire.', NULL),
('EVILR7', 'EVEILLOR', 'PSA', 'Adrafinil', 'Ce médicament est utilisé pour traiter les troubles de la vigilance et certains symptomes neurologiques chez le sujet agé.', 'Ce médicament est contre-indiqué en cas d\'allergie à  l\'un des constituants.', NULL),
('INSXT5', 'INSECTIL', 'AH', 'Diphénydramine', 'Ce médicament est utilisé en application locale sur les piqûres d\'insecte et l\'urticaire.', 'Ce médicament est contre-indiqué en cas d\'allergie aux antihistaminiques.', NULL),
('JOVAI8', 'JOVENIL', 'AFM', 'Josamycine', 'Ce médicament est utilisé pour traiter des infections bactériennes spécifiques.', 'Ce médicament est contre-indiqué en cas d\'allergie aux macrolides (dont le chef de file est l\'érythromycine).', NULL),
('LIDOXY23', 'LIDOXYTRACINE', 'AFC', 'Oxytétracycline +Lidocaïne', 'Ce médicament est utilisé en injection intramusculaire pour traiter certaines infections spécifiques.', 'Ce médicament est contre-indiqué en cas d\'allergie à  l\'un des constituants. Il ne doit pas être associé aux rétinoïdes.', NULL),
('LITHOR12', 'LITHORINE', 'AP', 'Lithium', 'Ce médicament est indiqué dans la prévention des psychoses maniaco-dépressives ou pour traiter les états maniaques.', 'Ce médicament ne doit pas être utilisé si vous êtes allergique au lithium. Avant de prendre ce traitement, signalez à  votre médecin traitant si vous souffrez d\'insuffisance rénale, ou si vous avez un régime sans sel.', NULL),
('PARMOL16', 'PARMOCODEINE', 'AA', 'Codéine + Paracétamol', 'Ce médicament est utilisé pour le traitement des douleurs lorsque des antalgiques simples ne sont pas assez efficaces.', 'Ce médicament est contre-indiqué en cas d\'allergie à  l\'un des constituants, chez l\'enfant de moins de 15 Kg, en cas d\'insuffisance hépatique ou respiratoire, d\'asthme, de phénylcétonurie et chez la femme qui allaite.', NULL),
('PHYSOI8', 'PHYSICOR', 'PSA', 'Sulbutiamine', 'Ce médicament est utilisé pour traiter les baisses d\'activité physique ou psychique, souvent dans un contexte de dépression.', 'Ce médicament est contre-indiqué en cas d\'allergie à  l\'un des constituants.', NULL),
('PIRIZ8', 'PIRIZAN', 'ABA', 'Pyrazinamide', 'Ce médicament est utilisé, en association à  d\'autres antibiotiques, pour traiter la tuberculose.', 'Ce médicament est contre-indiqué en cas d\'allergie à  l\'un des constituants, d\'insuffisance rénale ou hépatique, d\'hyperuricémie ou de porphyrie.', NULL),
('POMDI20', 'POMADINE', 'AO', 'Bacitracine', 'Ce médicament est utilisé pour traiter les infections oculaires de la surface de l\'oeil.', 'Ce médicament est contre-indiqué en cas d\'allergie aux antibiotiques appliqués localement.', NULL),
('TROXT21', 'TROXADET', 'AIN', 'Paroxétine', 'Ce médicament est utilisé pour traiter la dépression et les troubles obsessionnels compulsifs. Il peut également être utilisé en prévention des crises de panique avec ou sans agoraphobie.', 'Ce médicament est contre-indiqué en cas d\'allergie au produit.', NULL),
('TXISOL22', 'TOUXISOL Vitamine C', 'ALO', 'Tyrothricine + Acide ascorbique (Vitamine C)', 'Ce médicament est utilisé pour traiter les affections de la bouche et de la gorge.', 'Ce médicament est contre-indiqué en cas d\'allergie à  l\'un des constituants et chez l\'enfant de moins de 6 ans.', NULL),
('URIEG6', 'URIREGUL', 'AUM', 'Fosfomycine trométamol', 'Ce médicament est utilisé pour traiter les infections urinaires simples chez la femme de moins de 65 ans.', 'La prise de ce médicament est contre-indiquée en cas d\'allergie à  l\'un des constituants et d\'insuffisance rénale.', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `praticien`
--

DROP TABLE IF EXISTS `praticien`;
CREATE TABLE IF NOT EXISTS `praticien` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(25) NOT NULL,
  `prenom` varchar(25) NOT NULL,
  `adresse` varchar(255) NOT NULL,
  `id_Ville` int(11) DEFAULT NULL,
  `id_Type_Praticien` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_Praticien_id_Ville` (`id_Ville`),
  KEY `FK_Praticien_id_Type_Praticien` (`id_Type_Praticien`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `praticien`
--

INSERT INTO `praticien` (`id`, `nom`, `prenom`, `adresse`, `id_Ville`, `id_Type_Praticien`) VALUES
(1, 'Notini', 'Alain', '114 r Authie', 1, 1),
(2, 'Gosselin', 'Albert', '13 r Devon', 2, 2),
(3, 'Delahaye', 'André', '36 av 6 Juin', 3, 5),
(4, 'Leroux', 'André', '47 av Robert Schuman', 4, 3),
(5, 'Desmoulins', 'Anne', '31 r St Jean', 5, 4),
(6, 'Mouel', 'Anne', '27 r Auvergne', 6, 1),
(7, 'Desgranges-Lentz', 'Antoine', '1 r Albert de Mun', 7, 2),
(8, 'Marcouiller', 'Arnaud', '31 r St Jean', 8, 5),
(9, 'Dupuy', 'Benoit', '9 r Demolombe', 9, 3),
(10, 'Lerat', 'Bernard', '31 r St Jean', 10, 4),
(11, 'Marçais-Lefebvre', 'Bertrand', '86Bis r Basse', 11, 1),
(12, 'Boscher', 'Bruno', '94 r Falaise', 12, 2),
(13, 'Morel', 'Catherine', '21 r Chateaubriand', 13, 5),
(14, 'Guivarch', 'Chantal', '4 av Gén Laperrine', 14, 3),
(15, 'Bessin-Grosdoit', 'Christophe', '92 r Falaise', 15, 4),
(16, 'Rossa', 'Claire', '14 av Thiès', 15, 1),
(17, 'Cauchy', 'Denis', '5 av Ste Thérèse', 16, 2),
(18, 'Gaffé', 'Dominique', '9 av 1ère Armée Française', 17, 5),
(19, 'Guenon', 'Dominique', '98 bd Mar Lyautey', 18, 3),
(20, 'Prévot', 'Dominique', '29 r Lucien Nelle', 19, 4),
(21, 'Houchard', 'Eliane', '9 r Demolombe', 20, 1),
(22, 'Desmons', 'Elisabeth', '51 r Bernières', 21, 2),
(23, 'Flament', 'Elisabeth', '11 r Pasteur', 17, 5),
(24, 'Goussard', 'Emmanuel', '9 r Demolombe', 2, 3),
(25, 'Desprez', 'Eric', '9 r Vaucelles', 22, 4),
(26, 'Coste', 'Evelyne', '29 r Lucien Nelle', 23, 1),
(27, 'Lefebvre', 'Frédéric', '2 pl Wurzburg', 24, 2),
(28, 'Lemée', 'Frédéric', '29 av 6 Juin', 25, 5),
(29, 'Martin', 'Frédéric', 'Bât A 90 r Bayeux', 26, 3),
(30, 'Marie', 'Frédérique', '172 r Caponière', 26, 4),
(31, 'Rosenstech', 'Geneviève', '27 r Auvergne', 13, 1),
(32, 'Pontavice', 'Ghislaine', '8 r Gaillon', 27, 2),
(33, 'Leveneur-Mosquet', 'Guillaume', '47 av Robert Schuman', 28, 5),
(34, 'Blanchais', 'Guy', '30 r Authie', 29, 3),
(35, 'Leveneur', 'Hugues', '7 pl St Gilles', 30, 4),
(36, 'Mosquet', 'Isabelle', '22 r Jules Verne', 31, 1),
(37, 'Giraudon', 'Jean-Christophe', '1 r Albert de Mun', 32, 2),
(38, 'Marie', 'Jean-Claude', '26 r Hérouville', 33, 5),
(39, 'Maury', 'Jean-François', '5 r Pierre Girard', 34, 3),
(40, 'Dennel', 'Jean-Louis', '7 pl St Gilles', 35, 4),
(41, 'Ain', 'Jean-Pierre', '4 résid Olympia', 36, 1),
(42, 'Chemery', 'Jean-Pierre', '51 pl Ancienne Boucherie', 37, 2),
(43, 'Comoz', 'Jean-Pierre', '35 r Auguste Lechesne', 38, 5),
(44, 'Desfaudais', 'Jean-Pierre', '7 pl St Gilles', 39, 3),
(45, 'Phan', 'JérÃ´me', '9 r Clos Caillet', 40, 4),
(46, 'Riou', 'Line', '43 bd Gén Vanier', 41, 1),
(47, 'Chubilleau', 'Louis', '46 r Eglise', 42, 2),
(48, 'Lebrun', 'Lucette', '178 r Auge', 43, 5),
(49, 'Goessens', 'Marc', '6 av 6 Juin', 44, 3),
(50, 'Laforge', 'Marc', '5 résid Prairie', 45, 4),
(51, 'Millereau', 'Marc', '36 av 6 Juin', 46, 1),
(52, 'Dauverne', 'Marie-Christine', '69 av Charlemagne', 47, 2),
(53, 'Vittorio', 'Myriam', '3 pl Champlain', 48, 5),
(54, 'Lapasset', 'Nhieu', '31 av 6 Juin', 49, 3),
(55, 'Plantet-Besnier', 'Nicole', '10 av 1ère Armée Française', 50, 4),
(56, 'Chubilleau', 'Pascal', '3 r Hastings', 51, 1),
(57, 'Robert', 'Pascal', '31 r St Jean', 52, 2),
(58, 'Jean', 'Pascale', '114 r Authie', 53, 5),
(59, 'Chanteloube', 'Patrice', '14 av Thiès', 54, 3),
(60, 'Lecuirot', 'Patrice', 'résid St Pères 55 r Pigacière', 43, 4),
(61, 'Gandon', 'Patrick', '47 av Robert Schuman', 55, 1),
(62, 'Mirouf', 'Patrick', '22 r Puits Picard', 56, 2),
(63, 'Boireaux', 'Philippe', '14 av Thiès', 57, 5),
(64, 'Cendrier', 'Philippe', '7 pl St Gilles', 58, 3),
(65, 'Duhamel', 'Philippe', '114 r Authie', 9, 4),
(66, 'Grigy', 'Philippe', '15 r Mélingue', 59, 1),
(67, 'Linard', 'Philippe', '1 r Albert de Mun', 60, 2),
(68, 'Lozier', 'Philippe', '8 r Gaillon', 61, 5),
(69, 'Dechâtre', 'Pierre', '63 av Thiès', 62, 3),
(70, 'Goessens', 'Pierre', '22 r Jean Romain', 63, 4),
(71, 'Leménager', 'Pierre', '39 av 6 Juin', 64, 1),
(72, 'Née', 'Pierre', '39 av 6 Juin', 65, 2),
(73, 'Guyot', 'Pierre-Laurent', '43 bd Gén Vanier', 66, 5),
(74, 'Chauchard', 'Roger', '9 r Vaucelles', 54, 3),
(75, 'Mabire', 'Roland', '11 r Boutiques', 11, 4),
(76, 'Leroy', 'Soazig', '45 r Boutiques', 67, 1),
(77, 'Guyot', 'Stéphane', '26 r Hérouville', 68, 2),
(78, 'Delposen', 'Sylvain', '39 av 6 Juin', 69, 5),
(79, 'Rault', 'Sylvie', '15 bd Richemond', 70, 3),
(80, 'Renouf', 'Sylvie', '98 bd Mar Lyautey', 71, 4),
(81, 'Alliet-Grach', 'Thierry', '14 av Thiès', 72, 1),
(82, 'Bayard', 'Thierry', '92 r Falaise', 73, 2),
(83, 'Gauchet', 'Thierry', '7 r Desmoueux', 74, 5),
(84, 'Bobichon', 'Tristan', '219 r Caponière', 75, 3),
(85, 'Duchemin-Laniel', 'Véronique', '130 r St Jean', 76, 4),
(86, 'Laurent', 'Younès', '34 r Demolombe', 77, 1),
(87, 'Maertens', 'Corentin', '327 r lombarderie', 10, 1),
(88, 'Delattre', 'Pierre', '41 r girolles', 81, 1);

-- --------------------------------------------------------

--
-- Structure de la table `prescrire`
--

DROP TABLE IF EXISTS `prescrire`;
CREATE TABLE IF NOT EXISTS `prescrire` (
  `MED_DEPOTLEGAL` varchar(10) NOT NULL,
  `TIN_CODE` varchar(5) NOT NULL,
  `DOS_CODE` varchar(10) NOT NULL,
  `PRE_POSOLOGIE` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`MED_DEPOTLEGAL`,`TIN_CODE`,`DOS_CODE`),
  KEY `MED_DEPOTLEGAL` (`MED_DEPOTLEGAL`),
  KEY `TIN_CODE` (`TIN_CODE`),
  KEY `DOS_CODE` (`DOS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `presentation`
--

DROP TABLE IF EXISTS `presentation`;
CREATE TABLE IF NOT EXISTS `presentation` (
  `PRE_CODE` varchar(2) NOT NULL,
  `PRE_LIBELLE` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`PRE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `prestation`
--

DROP TABLE IF EXISTS `prestation`;
CREATE TABLE IF NOT EXISTS `prestation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `prestation`
--

INSERT INTO `prestation` (`id`, `nom`) VALUES
(1, 'visite'),
(2, 'conférence'),
(3, 'formation');

-- --------------------------------------------------------

--
-- Structure de la table `prospect`
--

DROP TABLE IF EXISTS `prospect`;
CREATE TABLE IF NOT EXISTS `prospect` (
  `id_Praticien` int(11) NOT NULL,
  `id_Etat` int(11) NOT NULL,
  PRIMARY KEY (`id_Praticien`),
  KEY `FK_Prospect_id_Etat` (`id_Etat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `prospect`
--

INSERT INTO `prospect` (`id_Praticien`, `id_Etat`) VALUES
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(53, 2),
(55, 2),
(56, 2),
(57, 2),
(59, 2),
(60, 2),
(61, 2),
(62, 2),
(52, 3),
(54, 3),
(58, 3);

-- --------------------------------------------------------

--
-- Structure de la table `rapport`
--

DROP TABLE IF EXISTS `rapport`;
CREATE TABLE IF NOT EXISTS `rapport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `motif` varchar(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `bilan` varchar(1000) DEFAULT NULL,
  `id_Praticien` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_Praticien` (`id_Praticien`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `rapport`
--

INSERT INTO `rapport` (`id`, `motif`, `date`, `bilan`, `id_Praticien`) VALUES
(1, 'Grippe', '2018-11-12', 'Principium autem unde latius se funditabat, emersit ex negotio tali. Chilo ex vicario et coniux eius Maxima nomine, questi apud Olybrium ea tempestate urbi praefectum, vitamque suam venenis petitam adseverantes inpetrarunt ut hi, quos suspectati sunt, ilico rapti conpingerentur in vincula, organarius Sericus et Asbolius palaestrita et aruspex Campensis.', 1),
(2, 'Gastro Entérite', '2018-11-01', 'Quam ob rem ut ii qui superiores sunt submittere se debent in amicitia, sic quodam modo inferiores extollere. Sunt enim quidam qui molestas amicitias faciunt, cum ipsi se contemni putant; quod non fere contingit nisi iis qui etiam contemnendos se arbitrantur; qui hac opinione non modo verbis sed etiam opere levandi sunt.\r\n\r\n', 4),
(3, 'Angine Blanche', '2018-11-24', 'Horum adventum praedocti speculationibus fidis rectores militum tessera data sollemni armatos omnes celeri eduxere procursu et agiliter praeterito Calycadni fluminis ponte, cuius undarum magnitudo murorum adluit turres, in speciem locavere pugnandi. neque tamen exiluit quisquam nec permissus est congredi. formidabatur enim flagrans vesania manus et superior numero et ruitura sine respectu salutis in ferrum.\r\n\r\n', 8),
(5, 'Rhumes', '1998-12-20', 'Horum adventum praedocti speculationibus fidis rectores militum tessera data sollemni armatos omnes celeri eduxere procursu et agiliter praeterito Calycadni fluminis ponte, cuius undarum magnitudo murorum adluit turres, in speciem locavere pugnandi. neque tamen exiluit quisquam nec permissus est congredi. formidabatur enim flagrans vesania manus et superior numero et ruitura sine respectu salutis in ferrum.\n\n', 7),
(6, 'Torticoli', '2019-03-03', 'Contraction musculaire du cou très douloureuse, qui empêche de tenir la tête droite.', 17),
(7, 'Tension élévé', '2019-03-11', 'Constatation d\'une tension élevé ( 14.5 )', 47),
(8, 'Tension élévé', '2019-03-11', 'Constatation d\'une tension élevé ( 14.5 )', 47),
(9, 'DISQUE VINCENT KC', '2019-03-15', 'il est dans la merde \nPRUDENCE !', 1);

-- --------------------------------------------------------

--
-- Structure de la table `session`
--

DROP TABLE IF EXISTS `session`;
CREATE TABLE IF NOT EXISTS `session` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `SES_UTILISATEUR` varchar(50) NOT NULL,
  `SES_PASSWORD` varchar(50) NOT NULL,
  `id_Praticien` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_Praticien` (`id_Praticien`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `session`
--

INSERT INTO `session` (`id`, `SES_UTILISATEUR`, `SES_PASSWORD`, `id_Praticien`) VALUES
(1, 'alain.notini', 'gsbcr', 1),
(6, 'albert.gosselin', 'gsbcr', 2),
(7, 'andre.delahaye', 'gsbcr', 3),
(8, 'andre.leroux', 'gsbcr', 4),
(9, 'anne.desmoumins', 'gsbcr', 5),
(10, 'anne.mouel', 'gsbcr', 6),
(11, 'antoine.desgranges-lentz', 'gsbcr', 7),
(12, 'arnaud.marcouiller', 'gsbcr', 8),
(13, 'benoit.dupuy', 'gsbcr', 9),
(14, 'bernard.lerat', 'gsbcr', 10),
(15, 'bertrand.marcais-lefebvre', 'gsbcr', 11),
(16, 'bruno.boscher', 'gsbcr', 12),
(17, 'catherine.morel', 'gsbcr', 13),
(18, 'chantal.guivarch', 'gsbcr', 14),
(19, 'christophe.bessin-grosdoit', 'gsbcr', 15),
(20, 'claire.rossa', 'gsbcr', 16),
(21, 'denis.cauchy', 'gsbcr', 17),
(22, 'dominique.gaffe', 'gsbcr', 18),
(23, 'dominique.guenon', 'gsbcr', 19),
(24, 'dominique.prevot', 'gsbcr', 20),
(25, 'eliane.houchard', 'gsbcr', 21),
(26, 'elisabeth.desmons', 'gsbcr', 22),
(27, 'elisabeth.flament', 'gsbcr', 23),
(28, 'emmanuel.goussard', 'gsbcr', 24),
(29, 'eric.desprez', 'gsbcr', 25),
(30, 'evelyn.coste', 'gsbcr', 26),
(31, 'frederic.lefebvre', 'gsbcr', 27),
(32, 'frederic.lemee', 'gsbcr', 28),
(33, 'frederic.martin', 'gsbcr', 29),
(34, 'frederique.marie', 'gsbcr', 30),
(35, 'genevieve.rosenstech', 'gsbcr', 31),
(36, 'ghislaine.pontavice', 'gsbcr', 32),
(37, 'guillaume.leveneur-mosquet', 'gsbcr', 33),
(38, 'guy.blanchais', 'gsbcr', 34),
(39, 'hugues.leveneur', 'gsbcr', 35),
(40, 'isabelle.mosquet', 'gsbcr', 36),
(41, 'jean-christophe.giraudon', 'gsbcr', 37),
(42, 'jean-claude.marie', 'gsbcr', 38),
(43, 'jean-francois.maury', 'gsbcr', 39),
(44, 'jean-louis.dennel', 'gsbcr', 40),
(45, 'jean-pierre.ain', 'gsbcr', 41),
(46, 'jean-pierre.chemery', 'gsbcr', 42),
(47, 'jean-pierre.comoz', 'gsbcr', 43),
(48, 'jean-pierre.desfaudais', 'gsbcr', 44),
(49, 'jerome.phan', 'gsbcr', 45),
(50, 'line.riou', 'gsbcr', 46),
(51, 'louis.chubilleau', 'gsbcr', 47),
(52, 'lucette.lebrun', 'gsbcr', 48),
(53, 'marc.goessens', 'gsbcr', 49),
(54, 'marc.laforge', 'gsbcr', 50),
(55, 'marc.millereau', 'gsbcr', 51),
(56, 'marie-christine.dauverne', 'gsbcr', 52),
(57, 'myriam.vittorio', 'gsbcr', 53),
(58, 'nhieu.lapasset', 'gsbcr', 54),
(59, 'nicole.plantet-besnier', 'gsbcr', 55),
(60, 'pascal.chubilleau', 'gsbcr', 56),
(61, 'pascal.robert', 'gsbcr', 57),
(62, 'pascale.jean', 'gsbcr', 58),
(63, 'patrice.chanteloube', 'gsbcr', 59),
(64, 'patrice.lecuirot', 'gsbcr', 60),
(65, 'patrick.gandon', 'gsbcr', 61),
(66, 'patrick.mirouf', 'gsbcr', 62),
(67, 'philippe.boireaux', 'gsbcr', 63),
(68, 'philippe.cendrier', 'gsbcr', 64),
(69, 'philippe.duhamel', 'gsbcr', 65),
(70, 'philippe.grigy', 'gsbcr', 66),
(71, 'philippe.linard', 'gsbcr', 67),
(72, 'philippe.lozier', 'gsbcr', 68),
(73, 'pierre.dechatre', 'gsbcr', 69),
(74, 'pierre.goessens', 'gsbcr', 70),
(75, 'pierre.lemenager', 'gsbcr', 71),
(76, 'pierre.nee', 'gsbcr', 72),
(77, 'pierre-laurent.guyot', 'gsbcr', 73),
(78, 'roger.chauchard', 'gsbcr', 74),
(79, 'roland.mabire', 'gsbcr', 75),
(80, 'soazig.leroy', 'gsbcr', 76),
(81, 'stephane.guyot', 'gsbcr', 77),
(82, 'sylvain.delposen', 'gsbcr', 78),
(83, 'sylvie.rault', 'gsbcr', 79),
(84, 'sylvie.renouf', 'gsbcr', 80),
(85, 'thierry.alliet-grach', 'gsbcr', 81),
(86, 'thierry.bayard', 'gsbcr', 82),
(87, 'thierry.gauchet', 'gsbcr', 83),
(88, 'tristan.bobichon', 'gsbcr', 84),
(89, 'veronique.duchemin-laniel', 'gsbcr', 85),
(90, 'younes.laurent', 'gsbcr', 86),
(91, 'corentin.maertens', 'gsbcr', 87),
(92, 'pierre.delattre', 'gsbcr', 88);

-- --------------------------------------------------------

--
-- Structure de la table `type_individu`
--

DROP TABLE IF EXISTS `type_individu`;
CREATE TABLE IF NOT EXISTS `type_individu` (
  `TIN_CODE` varchar(5) NOT NULL,
  `TIN_LIBELLE` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`TIN_CODE`),
  KEY `TIN_CODE` (`TIN_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `type_praticien`
--

DROP TABLE IF EXISTS `type_praticien`;
CREATE TABLE IF NOT EXISTS `type_praticien` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(6) NOT NULL,
  `libelle` varchar(25) NOT NULL,
  `lieu` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `type_praticien`
--

INSERT INTO `type_praticien` (`id`, `code`, `libelle`, `lieu`) VALUES
(1, 'MH', 'Médecin Hospitalier', 'Hopital ou Clinique'),
(2, 'MV', 'Médecine de Ville', 'Cabinet'),
(3, 'PH', 'Pharmacien Hospitalier', 'Hopital ou Clinique'),
(4, 'PO', 'Pharmacien Officine', 'Pharmacie'),
(5, 'PS', 'Personnel de santé', 'Centre Paramédical');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `identifiant` varchar(30) NOT NULL,
  `mot_de_passe` varchar(40) NOT NULL,
  `email` varchar(250) NOT NULL,
  `moment_connexion` datetime DEFAULT NULL,
  `ip_connexion` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `nom`, `prenom`, `identifiant`, `mot_de_passe`, `email`, `moment_connexion`, `ip_connexion`) VALUES
(1, 'Villechalane', 'Louis', 'lvillachane', 'jux7g', 'louis.villechalane@gsb.com', NULL, NULL),
(2, 'Andre', 'David', 'dandre', 'oppg5', 'david.andre@gsb.com', NULL, NULL),
(3, 'Bedos', 'Christian', 'cbedos', 'gmhxd', 'christian.bedos@gsb.com', NULL, NULL),
(4, 'Tusseau', 'Louis', 'ltusseau', 'ktp3s', 'louis.tusseau@gsb.com', NULL, NULL),
(5, 'Bentot', 'Pascal', 'pbentot', 'doyw1', 'pascal.bentot@gsb.com', NULL, NULL),
(6, 'Bioret', 'Luc', 'lbioret', 'hrjfs', 'luc.bioret@gsb.com', NULL, NULL),
(7, 'Bunisset', 'Francis', 'fbunisset', '4vbnd', 'francis.bunisset@gsb.com', NULL, NULL),
(8, 'Bunisset', 'Denise', 'dbunisset', 's1y1r', 'denise.bunisset@gsb.com', NULL, NULL),
(9, 'Cacheux', 'Bernard', 'bcacheux', 'uf7r3', 'bernard.cacheux@gsb.com', NULL, NULL),
(10, 'Cadic', 'Eric', 'ecadic', '6u8dc', 'eric.cadic@gsb.com', NULL, NULL),
(11, 'Charoze', 'Catherine', 'ccharoze', 'u817o', 'catherine.charoze@gsb.com', NULL, NULL),
(12, 'Clepkens', 'Christophe', 'cclepkens', 'bw1us', 'christophe.clepkens@gsb.com', NULL, NULL),
(13, 'Cottin', 'Vincenne', 'vcottin', '2hoh9', 'vincenne.cottin@gsb.com', NULL, NULL),
(14, 'Daburon', 'François', 'fdaburon', '7oqpv', 'françois.daburon@gsb.com', NULL, NULL),
(15, 'De', 'Philippe', 'pde', 'gk9kx', 'philippe.de@gsb.com', NULL, NULL),
(16, 'Debelle', 'Michel', 'mdebelle', 'od5rt', 'michel.debelle@gsb.com', NULL, NULL),
(17, 'Debelle', 'Jeanne', 'jdebelle', 'nvwqq', 'jeanne.debelle@gsb.com', NULL, NULL),
(18, 'Debroise', 'Michel', 'mdebroise', 'sghkb', 'michel.debroise@gsb.com', NULL, NULL),
(19, 'Desmarquest', 'Nathalie', 'ndesmarquest', 'f1fob', 'nathalie.desmarquest@gsb.com', NULL, NULL),
(20, 'Desnost', 'Pierre', 'pdesnost', '4k2o5', 'pierre.desnost@gsb.com', NULL, NULL),
(21, 'Dudouit', 'Frédéric', 'fdudouit', '44im8', 'frederic.dudouit@gsb.com', NULL, NULL),
(22, 'Duncombe', 'Claude', 'cduncombe', 'qf77j', 'claude.duncombe@gsb.com', NULL, NULL),
(23, 'Enault-Pascreau', 'Céline', 'cenault', 'y2qdu', 'celine.enault-pascreau@gsb.com', NULL, NULL),
(24, 'Eynde', 'Valérie', 'veynde', 'i7sn3', 'valerie.eynde@gsb.com', NULL, NULL),
(25, 'Finck', 'Jacques', 'jfinck', 'mpb3t', 'jacques.finck@gsb.com', NULL, NULL),
(26, 'Frémont', 'Fernande', 'ffremont', 'xs5tq', 'fernande.fremont@gsb.com', NULL, NULL),
(27, 'Gest', 'Alain', 'agest', 'dywvt', 'alain.gest@gsb.com', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur_groupe`
--

DROP TABLE IF EXISTS `utilisateur_groupe`;
CREATE TABLE IF NOT EXISTS `utilisateur_groupe` (
  `id_utilisateur` int(11) NOT NULL,
  `id_groupe` int(11) NOT NULL,
  PRIMARY KEY (`id_utilisateur`,`id_groupe`),
  KEY `utilisateur_groupe_ibfk_1` (`id_groupe`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateur_groupe`
--

INSERT INTO `utilisateur_groupe` (`id_utilisateur`, `id_groupe`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(6, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(13, 1),
(14, 1),
(16, 1),
(17, 1),
(19, 1),
(21, 1),
(23, 1),
(24, 1),
(26, 1),
(27, 1),
(5, 2),
(12, 2),
(18, 2),
(7, 3),
(15, 3),
(22, 3),
(25, 3),
(20, 4);

-- --------------------------------------------------------

--
-- Structure de la table `ville`
--

DROP TABLE IF EXISTS `ville`;
CREATE TABLE IF NOT EXISTS `ville` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) NOT NULL,
  `code_postal` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `ville`
--

INSERT INTO `ville` (`id`, `nom`, `code_postal`) VALUES
(1, 'LA ROCHE SUR YON', '85000'),
(2, 'BLOIS', '41000'),
(3, 'BESANCON', '25000'),
(4, 'BEAUVAIS', '60000'),
(5, 'NIMES', '30000'),
(6, 'AMIENS', '80000'),
(7, 'MORLAIX', '29000'),
(8, 'MULHOUSE', '68000'),
(9, 'MONTPELLIER', '34000'),
(10, 'LILLE', '59000'),
(11, 'STRASBOURG', '67000'),
(12, 'TROYES', '10000'),
(13, 'PARIS', '75000'),
(14, 'ORLEANS', '45000'),
(15, 'NICE', '6000'),
(16, 'NARBONNE', '11000'),
(17, 'RENNES', '35000'),
(18, 'NANTES', '44000'),
(19, 'LIMOGES', '87000'),
(20, 'ANGERS', '49100'),
(21, 'QUIMPER', '29000'),
(22, 'BORDEAUX', '33000'),
(23, 'TULLE', '19000'),
(24, 'VERDUN', '55000'),
(25, 'VANNES', '56000'),
(26, 'VESOUL', '70000'),
(27, 'POITIERS', '86000'),
(28, 'PAU', '64000'),
(29, 'SEDAN', '8000'),
(30, 'ARRAS', '62000'),
(31, 'ROUEN', '76000'),
(32, 'VIENNE', '38100'),
(33, 'LYON', '69000'),
(34, 'CHALON SUR SAONE', '71000'),
(35, 'CHARTRES', '28000'),
(36, 'LAON', '2000'),
(37, 'CAEN', '14000'),
(38, 'BOURGES', '18000'),
(39, 'BREST', '29000'),
(40, 'NIORT', '79000'),
(41, 'MARNE LA VALLEE', '77000'),
(42, 'SAINTES', '17000'),
(43, 'NANCY', '54000'),
(44, 'DOLE', '39000'),
(45, 'SAINT LO', '50000'),
(46, 'LA FERTE BERNARD', '72000'),
(47, 'DIJON', '21000'),
(48, 'BOISSY SAINT LEGER', '94000'),
(49, 'CHAUMONT', '52000'),
(50, 'CHATELLEREAULT', '86000'),
(51, 'AURRILLAC', '15000'),
(52, 'BOBIGNY', '93000'),
(53, 'SAUMUR', '49100'),
(54, 'MARSEILLE', '13000'),
(55, 'TOURS', '37000'),
(56, 'ANNECY', '74000'),
(57, 'CHALON EN CHAMPAGNE', '10000'),
(58, 'RODEZ', '12000'),
(59, 'CLISSON', '44000'),
(60, 'ALBI', '81000'),
(61, 'TOULOUSE', '31000'),
(62, 'MONTLUCON', '23000'),
(63, 'MONT DE MARSAN', '40000'),
(64, 'METZ', '57000'),
(65, 'MONTAUBAN', '82000'),
(66, 'MENDE', '48000'),
(67, 'ALENCON', '61000'),
(68, 'FIGEAC', '46000'),
(69, 'DREUX', '27000'),
(70, 'SOISSON', '2000'),
(71, 'EPINAL', '88000'),
(72, 'PRIVAS', '7000'),
(73, 'SAINT ETIENNE', '42000'),
(74, 'GRENOBLE', '38100'),
(75, 'FOIX', '9000'),
(76, 'LIBOURNE', '33000'),
(77, 'MAYENNE', '53000'),
(78, 'LILLE', '59000'),
(79, 'BOUVIGNIES', '59870'),
(80, 'WATTRELOS', '59150'),
(81, 'ORCHIES', '59310'),
(82, 'ORCHIES', '59310'),
(83, 'LILLE', '59000'),
(84, 'WATTRELOS', '59150'),
(85, 'ORCHIES', '59310');

-- --------------------------------------------------------

--
-- Structure de la table `visiteur`
--

DROP TABLE IF EXISTS `visiteur`;
CREATE TABLE IF NOT EXISTS `visiteur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `SES_UTILISATEURS` varchar(25) NOT NULL,
  `SES_PASSWORD` varchar(25) NOT NULL,
  `adresse` varchar(255) NOT NULL,
  `id_Ville` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_Visiteur_id_Ville` (`id_Ville`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `client`
--
ALTER TABLE `client`
  ADD CONSTRAINT `FK_Client_id_Praticien` FOREIGN KEY (`id_Praticien`) REFERENCES `praticien` (`id`);

--
-- Contraintes pour la table `constituer`
--
ALTER TABLE `constituer`
  ADD CONSTRAINT `constituer_ibfk_1` FOREIGN KEY (`CMP_CODE`) REFERENCES `composant` (`CMP_CODE`),
  ADD CONSTRAINT `constituer_ibfk_2` FOREIGN KEY (`MED_DEPOTLEGAL`) REFERENCES `medicament` (`MED_DEPOTLEGAL`);

--
-- Contraintes pour la table `formuler`
--
ALTER TABLE `formuler`
  ADD CONSTRAINT `formuler_ibfk_1` FOREIGN KEY (`MED_DEPOTLEGAL`) REFERENCES `medicament` (`MED_DEPOTLEGAL`),
  ADD CONSTRAINT `formuler_ibfk_2` FOREIGN KEY (`PRE_CODE`) REFERENCES `presentation` (`PRE_CODE`);

--
-- Contraintes pour la table `groupe_droit`
--
ALTER TABLE `groupe_droit`
  ADD CONSTRAINT `groupe_droit_ibfk_1` FOREIGN KEY (`id_droit`) REFERENCES `droit` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `groupe_droit_ibfk_2` FOREIGN KEY (`id_groupe`) REFERENCES `groupe` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `interagir`
--
ALTER TABLE `interagir`
  ADD CONSTRAINT `interagir_ibfk_1` FOREIGN KEY (`MED_MED_PERTURBE`) REFERENCES `medicament` (`MED_DEPOTLEGAL`),
  ADD CONSTRAINT `interagir_ibfk_2` FOREIGN KEY (`MED_PERTURBATEUR`) REFERENCES `medicament` (`MED_DEPOTLEGAL`);

--
-- Contraintes pour la table `interesser`
--
ALTER TABLE `interesser`
  ADD CONSTRAINT `FK_interesser_id_Prestation` FOREIGN KEY (`id_Prestation`) REFERENCES `prestation` (`id`),
  ADD CONSTRAINT `interesser_ibfk_1` FOREIGN KEY (`id_Client`) REFERENCES `client` (`id_Praticien`);

--
-- Contraintes pour la table `medicament`
--
ALTER TABLE `medicament`
  ADD CONSTRAINT `medicament_ibfk_1` FOREIGN KEY (`FAM_CODE`) REFERENCES `famille` (`FAM_CODE`);

--
-- Contraintes pour la table `praticien`
--
ALTER TABLE `praticien`
  ADD CONSTRAINT `FK_Praticien_id_Type_Praticien` FOREIGN KEY (`id_Type_Praticien`) REFERENCES `type_praticien` (`id`),
  ADD CONSTRAINT `FK_Praticien_id_Ville` FOREIGN KEY (`id_Ville`) REFERENCES `ville` (`id`);

--
-- Contraintes pour la table `prescrire`
--
ALTER TABLE `prescrire`
  ADD CONSTRAINT `prescrire_ibfk_1` FOREIGN KEY (`DOS_CODE`) REFERENCES `dosage` (`DOS_CODE`),
  ADD CONSTRAINT `prescrire_ibfk_2` FOREIGN KEY (`MED_DEPOTLEGAL`) REFERENCES `medicament` (`MED_DEPOTLEGAL`),
  ADD CONSTRAINT `prescrire_ibfk_3` FOREIGN KEY (`TIN_CODE`) REFERENCES `type_individu` (`TIN_CODE`);

--
-- Contraintes pour la table `prospect`
--
ALTER TABLE `prospect`
  ADD CONSTRAINT `FK_Prospect_id_Etat` FOREIGN KEY (`id_Etat`) REFERENCES `etat` (`id`),
  ADD CONSTRAINT `FK_Prospect_id_Praticien` FOREIGN KEY (`id_Praticien`) REFERENCES `praticien` (`id`);

--
-- Contraintes pour la table `rapport`
--
ALTER TABLE `rapport`
  ADD CONSTRAINT `rapport_ibfk_1` FOREIGN KEY (`id_Praticien`) REFERENCES `praticien` (`id`);

--
-- Contraintes pour la table `session`
--
ALTER TABLE `session`
  ADD CONSTRAINT `session_ibfk_1` FOREIGN KEY (`id_Praticien`) REFERENCES `praticien` (`id`);

--
-- Contraintes pour la table `utilisateur_groupe`
--
ALTER TABLE `utilisateur_groupe`
  ADD CONSTRAINT `utilisateur_groupe_ibfk_1` FOREIGN KEY (`id_groupe`) REFERENCES `groupe` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `utilisateur_groupe_ibfk_2` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `visiteur`
--
ALTER TABLE `visiteur`
  ADD CONSTRAINT `visiteur_ibfk_1` FOREIGN KEY (`id_Ville`) REFERENCES `ville` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

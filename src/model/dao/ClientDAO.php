<?php
/**
 * PHP version 7.0
 * gsb_prospects/src/model/dao/ClientDAO.php
 * 
 * @author  David RIEHL <david.riehl@ac-lille.fr>
 * @license GPL 3.0
 */
namespace gsb_prospects\model\dao;

use \PDO;
use \ReflectionClass;
use gsb_prospects\kernel\NotImplementedException;
use gsb_prospects\model\objects\Client;

/**
 * Class ClientDAO
 * 
 * @author  David RIEHL <david.riehl@ac-lille.fr>
 * @license GPL 3.0
 */
final class ClientDAO extends AbstractDAO implements IDAO
{
    protected $table = "client";
    protected $joinedTables = [
        [ "Type"=>"Inner", "Table"=>"praticien", "Foreign Table"=>"client", "Foreign Key"=>["id_praticien"], "Primary Table"=>"praticien", "Primary Key"=>["id"] ],
    ];
    protected $class = "gsb_prospects\model\objects\Client";
    protected $fields = [
        "id_Praticien", "id", "nom", "prenom", "adresse", "id_Ville", "id_Type_Praticien"
    ];
    
    /**
     * Function delete
     * Generate a DELETE FROM query to delete an object from a table
     *
     * @param object $object
     *
     * Currently, it is not implemented
     */
    public function delete(&$object)
    {
        throw new NotImplementedException();
    }

	/**
     * Function findAll
     * Generate a SELECT query to find all items from a table
     *
     * @return array collection of objects instanceof $this->class
    */
    public function findAll()
    {
        $dbh = $this->getConnexion();

        $query  = "SELECT * FROM `{$this->table}`" . PHP_EOL;
        if (! empty($this->joinedTables)) {
            $query .= $this->join();
        }
        
        $sth = $dbh->prepare($query);
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute();

        $array = $sth->fetchAll();

        $this->closeConnexion();

        if ($array === false) {
            $message = $sth->errorInfo()[2];    // Error Message
            $code = $sth->errorInfo()[0];       // SQLSTATE
            if ($code != 0) {
                throw new DAOException($message, $code);
            }
        } else {
            $objects = [];
            foreach ($array as $row) {
                $reflectedClass = new ReflectionClass($this->class);
                $object = $reflectedClass->newInstanceArgs($row);
                $objects[] = $object;
            }
        }

        return $objects;
    }

	/**
     * Function insert
     * 
	 *This function can be used to insert something on our database. It takes as parameter an object. Currently, it is not implemented
	 *
	 *@return an Exception if we use it
     */
    public function insert(&$object)
    {
        throw new NotImplementedException();
    }

	/**
     * Function update
     * 
	 *This function can be used to update something on our database. It takes as parameter an object. Currently, it is not implemented
	 *
	 *@return an Exception if we use it
     */
    public function update($object)
    {
        throw new NotImplementedException();
    }
}
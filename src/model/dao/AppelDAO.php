<?php
/**
 * File :        AppelDAO.php
 * Location :    gsb_prospects/src/model/dao/AppelDAO.php
 * PHP Version : 7.0
 * 
 * @author  David RIEHL <david.riehl@ac-lille.fr>
 * @license GPL 3.0
 */
namespace gsb_prospects\model\dao;

use \PDO;
use gsb_prospects\kernel\NotImplementedException;
use gsb_prospects\model\objects\Appel;

/**
 * Class AppelDAO
 * 
 * @author  David RIEHL <david.riehl@ac-lille.fr>
 * @license GPL 3.0
 */
final class AppelDAO extends AbstractDAO implements IDAO
{
    protected $table = "appel";
    protected $class = "gsb_prospects\model\objects\Appel";
    protected $fields = [ 
        "id", "motif", "description", "dateHeure", "id_Praticien" 
    ];
}
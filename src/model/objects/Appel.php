<?php
/**
 * File :        Appel.php
 * Location :    gsb_prospects/src/model/objects/Appel.php
 * PHP Version : 7.0
 * 
 * @author  David RIEHL <david.riehl@ac-lille.fr>
 * @license GPL 3.0
 */
namespace gsb_prospects\model\objects;

use gsb_prospects\model\dao\VilleDAO;
use gsb_prospects\model\objects\Ville;
use gsb_prospects\model\objects\TypePraticien;

/**
 * Class Appel
 * 
 * @author  David RIEHL <david.riehl@ac-lille.fr>
 * @license GPL 3.0
 */
class Appel extends AbstractObject
{
    /**
     * Properties
     *
     * @var int    $id
     * @var string $motif
     * @var string $description
     * @var string $date/heure
     * @var object $lePraticien
     */
    protected $id;
    protected $motif;
    protected $description;
    protected $dateHeure;
    protected $id_Praticien;
    protected $lePraticien;

    /* Methods */

    /**
     * __construct
     *
     * @param int    $id                id
     * @param string $motif             motif
     * @param string $description       description
     * @param string $date/heure        date/heure
     * @param int    $id_Praticien      id_Praticien (default:null)
     */
    public function __construct($id, $motif, $description, $dateHeure, $id_Praticien = null)
    {
        $this->id                = $id;
        $this->motif             = $motif;
        $this->description        = $description;
        $this->dateHeure           = $dateHeure;
        $this->id_Praticien = $id_Praticien;
        $this->lePraticien   = null;
    }

    /**
     * Function getId
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Procedure setId
     *
     * @param string $value value
     *
     * @return void
     */
    public function setId(string $value)
    {
        $this->id = $value;
    }
 
    /**
     * Function getMotif
     * 
     * @return string
     */
    public function getMotif(): string
    {
        return $this->motif;
    }

    /**
     * Procedure setMotif
     *
     * @param string $value value
     *
     * @return void
     */
    public function setMotif(string $value)
    {
        $this->motif = $value;
    }

    /**
     * Function getDescription
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * Procedure setDescription
     *
     * @param string $value value
     *
     * @return void
     */
    public function setDescription(string $value)
    {
        $this->description = $value;
    }

    /**
     * Function getDateHeure
     *
     * @return string
     */
    public function getDateheure(): string
    {
        return $this->dateHeure;
    }

    /**
     * Procedure setDate/Heure
     *
     * @param string $value value
     *
     * @return void
     */
    public function setDateheure(string $value)
    {
        $this->dateHeure = $value;
    }

    /**
     * Function getIdPraticien
     *
     * @return int
     */
    public function getIdPraticien()
    {
        return $this->id_Praticien;
    }

    /**
     * Function getPraticien
     *
     * @return object
     */
    public function getPraticien()
    {
        return $this->lePraticien;
    }

    /**
     * Procedure setPraticien
     *
     * @param object $instance instanceof Praticien
     *
     * @return void
     */
    public function setPraticien(Praticien $instance)
    {
        $this->lePraticien = $instance;
    }
}
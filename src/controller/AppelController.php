<?php
/**
 * File :        PraticienController.php
 * Location :    gsb_prospects/src/controller/PraticienController.php
 * PHP Version : 7.0
 * 
 * @author  David RIEHL <david.riehl@ac-lille.fr>
 * @license GPL 3.0
 */
namespace gsb_prospects\controller;

use gsb_prospects\kernel\Route;
use gsb_prospects\kernel\Router;
use gsb_prospects\model\dao\AppelDAO;
use gsb_prospects\model\objects\Appel;
use gsb_prospects\view\View;

/**
 * Class PraticienController
 * 
 * @author  David RIEHL <david.riehl@ac-lille.fr>
 * @license GPL 3.0
 */
final class AppelController extends AbstractController implements IController
{
    /**
     * __construct
     */
    public function __construct()
    {
        $this->_dao = new AppelDAO();
        $this->_router = new Router();
        // 2nd level route definition
        $this->_router->addRoute(new Route("/appels", "AppelController", "listAction", "appel_list"));
        $this->_router->addRoute(new Route("/appels/create", "AppelController", "createAction", "appel_create"));
    }

    /**
     * Procedure defaultAction
     *
     * @return void
     */
    public function defaultAction()
    {
        $route = $this->_router->findRoute();
        if ($route) {
            $route->execute();
        } else {
            print("<p> Page inconnue.</p>" . PHP_EOL);
        }
    }

    /**
     * Procedure listAction
     *
     * @return void
     */
    public function listAction()
    {
        $view = new View("Appel_List");

        $view->bind("title", "Liste des Appels");
        $view->bind("objectName", "appel");
        $view->bind("objectNamePlural", "appels");

        $basePath = $this->_router->getBasePath();
        $view->bind("basePath", $basePath);

        $objects = $this->_dao->findAll();
        $view->bind("objects", $objects);

        $view->display();
    }

    /**
     * Procedure createAction
     *
     * @return void
     */
    public function createAction()
    {
        $view = new View("Appel_Create");

        $view->bind("title", "Ajout d'un Appel");

        $basePath = $this->_router->getBasePath();
        $view->bind("basePath", $basePath);

        $view->display();
    }
}